<section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-9 ">
			  <div class="border-head">
					  <br>
					  <br>
				  <h3>Dashboard</h3>
				</div>
              <!-- /col-md-4 -->
              <div class="col-md-5 col-sm-5 mb">
                <!-- WHITE PANEL - TOP USER -->
                <div class="white-panel pn">
                  <div class="white-header">
                    <h5>Projects- Current Financial Year (2019-2020)</h5>
                  </div>
                  <table class="table">
					<tbody>
					<tr>
						<th>Description</th>
						<th>No </th>
						<th>Amount </th>
					   </tr>
					  <tr>
						<td>DPR</td>
						<td> <?php echo $dprno_1920 ?></td>
						<td> <?php echo $dpramt_1920 ?></td>
					  </tr>
					  <tr>
						<td>AS</td>
						<td> <?php echo $asno_1920 ?></td>
						<td> <?php echo $asamt_1920 ?></td>
					  </tr>
					  <tr>
						<td>TS</td>
						<td> <?php echo $tsno_1920 ?></td>
						<td> <?php echo $tsamt_1920 ?></td>
					  </tr>
					  <tr>
						<td>Tendered</td>
						<td> <?php echo $tdrno_1920 ?></td>
						<td> <?php echo $tdramt_1920 ?></td>
					  </tr>
					  <tr>
						<td>Work awarded</td>
						<td> <?php echo $awdno_1920 ?></td>
						<td> <?php echo $awdamt_1920 ?></td>
					  </tr>
					</tbody>
					
              </table>
                  
                
              </div>
              <!-- /col-md-4 -->
             
              </div>
              <!-- /col-md-4 -->
            </div>
            <!-- /row -->
			
			
			
          </div>
          <!-- /col-lg-9 END SECTION MIDDLE -->
          
      </section>
    </section>