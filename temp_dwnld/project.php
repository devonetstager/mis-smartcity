<?php
include("dbconfig/dbconfig.php");

$url_string1 = $_GET['link'];
$arr1 = explode(":",$_GET['link']);
$sub_pid =  $arr1[0];
$p_id = $arr1[1];
//echo $sub_pid;

/**
 * if (!isset($_GET["link2"])) {
 *     $sub_pid = "NIL";   
 * }
 */
//echo $sub_pid." ** ".$p_id;

?>
<?php
    include("search_all_query.php");
    include("execQuery.php");
    include("dprQuery.php");
    include("tsQuery.php");
    include("tenderQuery.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>SCTL - Project</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
 
  <link href="lib/fancybox/jquery.fancybox.css" rel="stylesheet" />
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.mySlides {display:none;}
</style>
<style>
      .zoomA {
		  z-index: 1;
        width: 200 px;
        height: auto;
        transition-duration: 1s;
        transition-timing-function: ease;
      }
      .zoomA:hover {
        transform: scale(3);
      }
    </style>

  <!-- =======================================================
  
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg" style="background-color: #0171c7;">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>SCTL</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        
        <!--  notification end -->
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <?php $userName = "User";//$_SESSION["name"]; ?>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="#"><img src="img/sctl_logo.png" class="img-circle" width="80"></a></p>
          <h5 class="centered"><?php echo "User"; ?></h5>
          <li class="mt">
            <a href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
		  <li class="mt">
            <a href="projectRsearchAll.php" class="active">
              <i class="fa fa-dashboard"></i>
              <span>All Projects</span>
              </a>
          </li>
		  
		  
         
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3></h3>
        <div class="row mt">
          <div class="col-lg-13">
            
            <?php
			
					include("search_all_query.php");
					
					
				//	$temp = explode(":",$p_id);
				//	$p_id = $temp[1];
		          	$sub_pid =  $arr1[0];
                    $p_id = $arr1[1];
					$sqlProjfund = "SELECT * FROM project_details_with_fund WHERE p_id='".$p_id."'";
					
					
					//echo $sql;
					$result1 = mysqli_query($conn, $sqlProjfund);
					if(mysqli_num_rows($result1) > 0) {
						$row = mysqli_fetch_assoc($result1);
						$projectName = $row['p_id']."-".$row['p_name'];
						$estCost = round($row['p_estcost'],2);
						$scGrant = round($row['p_grant'],2);
						$cScheme = round($row['p_csp'],2);
						$ppp = round($row['p_ppp'],2);
						$ulbFund = round($row['p_ulb'],2);
						
						}
						
						$sqlas = "SELECT * FROM as_approved WHERE p_id='".$_GET['link']."'";
					//echo $sql;
					$result2 = mysqli_query($conn, $sqlas);
					if(mysqli_num_rows($result2) > 0) {
						$row = mysqli_fetch_assoc($result2);
						$asOrdernum = $row['as_orderNo'];
						$asOrderdate = $row['as_date'];
						$p_type = $row['p_type'];
						$asAmount = $row['as_amt'];
						}
						
						$sqlts = "SELECT * FROM ts_approved WHERE p_id='".$_GET['link']."'";
					//echo $sql;
					$result3 = mysqli_query($conn, $sqlts);
					if(mysqli_num_rows($result3) > 0) {
						$row = mysqli_fetch_assoc($result3);
						$tsOrdernum = $row['ts_ordernum'];
						$tsDate = $row['ts_date'];
						$p_type = $row['p_type'];
						$tsAmount = $row['ts_amt'];
						}
						
						$sqlwa = "SELECT * FROM work_awarded WHERE sub_pid='".$_GET['link']."-1'";
					//echo $sqlwa;
					$result4 = mysqli_query($conn, $sqlwa);
					
					if(mysqli_num_rows($result4) > 0) {
					
						$row = mysqli_fetch_assoc($result4);
						$ctrName = $row['wa_name'];
						$ctrAdd = $row['wa_add'];
						$waAmount = $row['wa_amt'];
						$pStartdate = $row['wa_sdate'];
						$pEnddate = $row['wa_cdate'];
						$wStatus = $row['wa_status'];
						//echo $row['wa_name'];
						}
						
						$sqlallp = "SELECT * FROM allprojects WHERE p_id='".$p_id."'";
					//echo $sqlallp;
					$resultallp = mysqli_query($conn, $sqlallp);
					
					if(mysqli_num_rows($resultallp) > 0) {
					
						$rowall = mysqli_fetch_assoc($resultallp);
						
						//echo $row['wa_name'];
						}
						
						
						
			
			?>
			<div class="col-lg-12">
              <div class="col-md-4 col-sm-4 mb" style="width:100%">
                  <div class="green-panel">
                    <div class="green-header" style="background-color: #0090ff;">
                      <h3 style="font-weight: bold; color: black;"><?php echo $projectName; ?></h3>
                      <h4><?php if($sub_pid) {$sqlsubp = "SELECT * FROM allprojects WHERE sub_pid='".$sub_pid."'";
        					$resultsubp = mysqli_query($conn, $sqlsubp);
        					if(mysqli_num_rows($resultsubp) > 0) {
        						$rowallsubp = mysqli_fetch_assoc($resultsubp);
						}echo $rowallsubp["sub_pname"];} ?></h4>
                       
                    </div>
                  </div>
              </div>
            </div>
               
               <div class="col-lg-12" style="margin-top: -50px;">
                   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                   
                    <div class="custom-box">
                      <!-- WHITE PANEL - TOP USER -->
                      <div class="white-panel">
                        <div class="white-header" style="background-color: #0090ff;">
                          <h4 style="font-size: large; font-weight: bolder;">Project Details</h4>
                        </div>
                        <!-- Work awarded row 1 -->
                          <div class="grey-panel" style="background-color: #f3f3f7;/* padding-top: 5px; */margin-top: 10px;">
                          
                                                      
                            <div class="row" style="margin: 0px;box-shadow: 0px 3px 2px #aab2bd;">
                             <table class="table table-bordered table-condensed">
                                     <thead>
                                      <tr style="text-align: center;background-color: darkgray;">
                                        <th style="text-align: center;">Estimated Cost<br><div style="font-size: smaller;">(In Rs. Cr)</div></th>
                                        <th style="text-align: center;">Smart City Grant<br><div style="font-size: smaller;">(In Rs. Cr)</div></th>
                                        <th style="text-align: center;">PPP<br><div style="font-size: smaller;">(In Rs. Cr)</div></th>
                                        <th style="text-align: center;">ULB Fund <br><div style="font-size: smaller;">(In Rs. Cr)</div> </th>
                                        <th style="text-align: center;">Central Scheme <br><div style="font-size: smaller;">(In Rs. Cr)</div> </th>
                                        
                                      </tr>
                                    </thead>
                                <tbody style="text-align: left;">
                                  <tr style="text-align: center;">
                                    <td style="text-align: center;"><?php echo $estCost; ?></td>
                                    <td style="text-align: center;"><?php echo $ppp; ?></td>
                                    <td style="text-align: center;"><?php echo $ulbFund; ?></td>
                                    <td style="text-align: center;"><?php echo $ulbFund; ?></td>
                                    <td style="text-align: center;"><?php echo $cScheme; ?></td>
                                   
                                  </tr>
                                  
                                </tbody>
                                
                              </table>
                              <br />
                              <table class="table table-bordered table-condensed">
                                     <thead>
                                      <tr style="text-align: center;background-color: darkgray;">
                                        <th style="text-align: center;">Ward Name & No.</th>
                                        <th style="text-align: center;">Project Status</th>
                                        <th style="text-align: center;">SCTL Official in Charge</th>
                                        <th style="text-align: center;">PMC Official in Charge</th>
                                        
                                      </tr>
                                    </thead>
                                <tbody style="text-align: left;">
                                  <tr style="text-align: center;">
                                    <td style="text-align: center;"><?php echo $rowall['ward_name']; ?></td>
                                    <td style="text-align: center;"><?php echo $rowall['p_status']; ?></td>
                                    <td style="text-align: center;"><?php echo $sctlOfficial; ?></td>
                                    <td style="text-align: center;"><?php echo $pmcOfficial; ?></td>
                                   
                                  </tr>
                                  
                                </tbody>
                                
                              </table>
                              <h4 style="text-align: left;"><i class="fa fa-angle-right" style="text-align: left;"></i> Project Progress: <?php if($as_amount>0){echo round(($wc_amt/$as_amount)*100,2)."%";} else echo "0 %";?></h4>
                              <div class="progress progress-striped active">
                              
                <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: <?php if($as_amount>0){echo round(($wc_amt/$as_amount)*100,2)."%";} else echo "0 %";?>; background-color: #b73333;">
                  <span class="sr-only"><?php if($as_amount>0){echo round(($wc_amt/$as_amount)*100,2)."%";} else echo "0 %";?>Complete</span>
                </div>
              </div>
                              </div>
                                                      
                            
                                                      
                           
                        
                          </div>
                            <!-- row 1 -->
                      </div>
                     </div>
                    
                        <!-- end custombox -->
                        
                      
                  </div>
                  <!-- end col-8 -->
                  
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="custom-box">
					<div class="w3-content w3-section" style="max-width:500px">
					<?php
					if($subprojectID){
					   $sqlimg = "SELECT * FROM project_images WHERE sub_pid='".$subprojectID."' ";
                       }else
                       $sqlimg = "SELECT * FROM project_images WHERE p_id='".$projectID."' ";
                    
					//echo $sqlimg;
					$resultimg = mysqli_query($conn, $sqlimg);

					if (mysqli_num_rows($resultimg) > 0) {
						// output data of each row
						while($rowimg = mysqli_fetch_assoc($resultimg)) {
						echo "<img class='mySlides' src='project_update/".$rowimg['img_root']."' style='width:100%'>";
						}
						}
					?>
					
				  
				</div>
                     <!-- <img src="img/ui-zac.jpg" height = "204" width ="300"></p>-->
                    </div>
                    <!-- end custombox -->
                  </div>


                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  style="margin-top: -50px;">
                   <div class="custom-box"> 
                      <!-- WHITE PANEL - TOP USER -->
                      <div class="white-panel">
                        
                        <div class="row black" style="margin-left: 0px; margin-right: 0px;">
                          <!-- row 1 -->
                          <div class="grey-panel" style="background-color: #f3f3f7;/* padding-top: 5px; */margin-top: 10px;">
                          <div class="grey-header" style="background-color: #0090ff;">
                            <h5 style="text-align: left;font-weight: 750;">Administrative Sanction</h5>
                          </div>
                         <div class="row" style="margin: 0px;box-shadow: 0px 3px 2px #aab2bd;">
                             <table class="table table-bordered table-condensed">
                                     <thead style="background-color: darkgray;">
                                      <tr>
                                        <th style="text-align: center;">Amount</th>
                                        <th style="text-align: center;">Order No</th>
                                        <th style="text-align: center;">Date</th>
                                        <th style="text-align: center;">Status</th>
                                      </tr>
                                    </thead>
                                <tbody style="text-align: left;">
                                  <tr>
                                    
                                    <td style="text-align: center;"><?php echo $as_amount; ?></td>
                                    <td style="text-align: center;"><?php echo $as_orderno; ?></td>
                                    <td style="text-align: center;"><?php echo $as_date; ?></td>
                                    <td style="text-align: center;"><?php echo $as_status;?></td>
                                  </tr>
                                </tbody>
                                
                              </table>
                              </div>
                          </div>
                            <!-- row 1 -->
                            
                            
                            <?php 
                            include("dprQuery.php");
                            ?>
                            <!-- dpr row 1 -->
                          <div class="grey-panel" style="background-color: #f3f3f7;/* padding-top: 5px; */margin-top: 10px;">
                          <div class="grey-header" style="background-color: #0090ff;">
                            <h5 style="text-align: left;font-weight: 750;">DPR Details</h5>
                          </div>
                                                      
                            <div class="row" style="margin: 0px;">
                             <table class="table table-bordered table-condensed">
                                     <thead style="background-color: darkgray;">
                                      <tr>
                                        <th style="text-align: center;">Amount</th>                                        
                                        <th style="text-align: center;">Type: Doc. No & Date</th>
                                        <th style="text-align: center;">Status</th>
                                        <th style="text-align: center;">Remark</th>
                                      </tr>
                                    </thead>
                                <tbody style="text-align: left;">
                                  <tr>
                                    <td style="text-align: center;"><?php echo $dpr_amt;?></td>
                                    <td style="text-align: center;"><?php echo $dpr_details;?></td>
                                    <td style="text-align: center;"><?php echo $dpr_status;?></td>
                                    <td style="text-align: center;"><?php echo $dpr_remark;?></td>
                                  </tr>
                                </tbody>
                                
                              </table>
                              </div>
                           
                        
                          </div>
                            <!-- row 1 -->
                            
                            <?php 
                            include("tsQuery.php");
                            ?>
                            <!-- TS row 1 -->
                          <div class="grey-panel" style="background-color: #f3f3f7;/* padding-top: 5px; */margin-top: 10px;">
                          <div class="grey-header" style="background-color: #0090ff;">
                            <h5 style="text-align: left;font-weight: 750;">Technical Sanction</h5>
                          </div>
                                                      
                            <div class="row" style="margin: 0px;">
                             <table class="table table-bordered table-condensed">
                                     <thead style="background-color: darkgray;">
                                      <tr>
                                        
                                        <th style="text-align: center;">Amount</th>
                                        <th style="text-align: center;">Order No & Date</th>
                                        <th style="text-align: center;">Status</th>
                                        <th style="text-align: center;">Remarks</th>
                                      </tr>
                                    </thead>
                                <tbody style="text-align: left;">
                                  <tr>
                                    <td style="text-align: center;"><?php echo $ts_amt;?></td>
                                    <td style="text-align: center;"><?php echo $ts_orderno."".$ts_date;?></td>
                                    <td style="text-align: center;"><?php echo $ts_status;?></td>                                    
                                    <td style="text-align: center;"><?php echo $ts_remark;?></td>
                                  </tr>
                                </tbody>
                                
                              </table>
                              </div>
                                                    
                          </div>
                            <!-- row 1 -->
                            <?php 
                            include("tenderQuery.php");
                            ?>
                            <!-- tender row 1 -->
                          <div class="grey-panel" style="background-color: #f3f3f7;/* padding-top: 5px; */margin-top: 10px;">
                          <div class="grey-header" style="background-color: #0090ff;">
                            <h5 style="text-align: left;font-weight: 750;">Tender Details</h5>
                          </div>
                                                      
                            <div class="row" style="margin: 0px;">
                             <table class="table table-bordered table-condensed">
                                     <thead style="background-color: darkgray;">
                                      <tr>
                                        <th style="text-align: center;">PAC Amount</th>
                                        <th style="text-align: center;">Start Date</th>
                                        <th style="text-align: center;">End Date</th>
                                        <th style="text-align: center;">Pre Bid Date</th>
                                        <th style="text-align: center;">Status</th>
                                        <th style="text-align: center;">Remarks</th>
                                      </tr>
                                    </thead>
                                <tbody style="text-align: left;">
                                  <tr>
                                    <td style="text-align: center;"><?php echo $tender_amt;?></td>
                                    <td style="text-align: center;"><?php echo $tender_sdate;?></td>
                                    <td style="text-align: center;"><?php echo $tender_cdate;?></td>
                                    <td style="text-align: center;"><?php echo $tender_prebid;?></td>
                                    <td style="text-align: center;"><?php echo $tender_status;?></td>
                                    <td style="text-align: center;"><?php echo $tender_remark;?></td>
                                  </tr>
                                </tbody>
                                
                              </table>
                              </div>
                                                   
                          </div>
                            <!-- row 1 -->
                            <?php 
                            include("execQuery.php");
                            ?>
                            <!-- Work awarded row 1 -->
                          <div class="grey-panel" style="background-color: #f3f3f7;/* padding-top: 5px; */margin-top: 10px;">
                          <div class="grey-header" style="background-color: #0090ff;">
                            <h5 style="text-align: left;font-weight: 750;">Execution Details</h5>
                          </div>
                                                      
                            <div class="row" style="margin: 0px;box-shadow: 0px 3px 2px #aab2bd;">
                             <table class="table table-bordered table-condensed">
                                     <thead style="background-color: darkgray;">
                                      <tr>
                                        
                                        <th style="text-align: center;">Work Award Amount</th>
                                        <th style="text-align: center;">Completion Amount</th>
                                        <th style="text-align: center;">Executing Agency</th>
                                        <th style="text-align: center;">Start Date</th>
                                        <th style="text-align: center;">End Date</th>                                        
                                        <th style="text-align: center;">Status</th>
                                        
                                        </tr>
                                    </thead>
                                  
                                <tbody style="text-align: left;">
                                  <tr>
                                    <td style="text-align: center;"><?php  echo $exec_amt; ?></td>
                                    <td style="text-align: center;"><?php  echo $wc_amt; ?></td>
                                    <td style="text-align: center;"><?php  echo $exec_agency; ?></td>
                                    <td style="text-align: center;"><?php  echo $exec_sdate; ?></td>
                                    <td style="text-align: center;"><?php  echo $exec_edate; ?></td>
                                    <td style="text-align: center;"><?php  echo $exec_status; ?></td>
                                     </tr>
                                  </tbody>
                                
                              </table>
                              
                              <table class="table table-bordered table-condensed">
                                     <thead style="background-color: darkgray;">
                                      <tr>
                                        
                                        <th style="text-align: center;">Civil Work</th>
                                        <th style="text-align: center;">Electrical Work</th>
                                        <th style="text-align: center;">ICT Work</th>
                                        <th style="text-align: center;">Remarks</th>
                                        <th style="text-align: center;">Phy Progress</th>
                                        <th style="text-align: center;">Fin Progress</th>
                                      </tr>
                                    </thead>
                                  
                                <tbody style="text-align: left;">
                                  <tr>
                                    
                                    <td style="text-align: center;"><?php  echo $exec_civil; ?></td>
                                    <td style="text-align: center;"><?php  echo $exec_ele; ?></td>
                                    <td style="text-align: center;"><?php  echo $exec_ict; ?></td>
                                    <td style="text-align: center;"><?php  echo $exec_remark; ?></td>
                                    <td style="text-align: center;"><?php  echo $pw_prog; ?></td>
                                    <td style="text-align: center;"><?php  echo $exp_prog; ?></td>
                                  </tr>
                                  </tbody>
                                
                              </table>
                              
                              <table class="table table-bordered table-condensed">
                                     <thead style="background-color: darkgray;">
                                      <tr>
                                        
                                        <th style="text-align: center;">Work Completion Date</th>
                                       </tr>
                                    </thead>
                                  
                                <tbody style="text-align: left;">
                                  <tr>
                                    
                                    <td style="text-align: center;"><?php  echo $wc_date; ?></td>
                                    </tr>
                                  </tbody>
                                
                              </table>
                              
                              </div>
                                                      
                            
                                                      
                           
                        
                          </div>
                            <!-- row 1 -->
                         
						  
						  
						  
						  
                        </div>
                        
                       
                        
						                        
						                      
                        <div class="row" style="margin-top: 20px;">
                          <div class="col-md-12">
                            <p class="" style="color:#000; font-weight: bolder;"  >PROJECT COMPONENTS/DESCRIPTION</p>
                            <p class="form-control" rows="10" cols="30" id="p_description" name="p_description" required="" value="<?php echo $pDescription; ?>"><?php echo $pDescription; ?></p>
                            
                          </div>
                          
                        </div>
                     </div>
                        
                    </div>
                 </div>
                        <!-- end custombox -->
            </div>
                  <!-- end col-12 -->

                  
                  
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
                        <!-- end custombox -->
          </div>
                  <!-- end col-8 -->
                  
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -50px;">
                    
                     <div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading">
                <ul class="nav nav-tabs nav-justified">
                  <li class="active">
                    <a data-toggle="tab" href="#overview">IMAGES</a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#contact" class="contact-map">DOCUMENTS</a>
                  </li>                 
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body">
                <div class="tab-content">
                  <div id="overview" class="tab-pane active">
                  
                  
                  
                  
                  <?php 
                  include("image_pane_gallery.php");
                  ?>
                  
                  </div>
                  <!-- /tab-pane -->
                  <div id="contact" class="tab-pane">
                   
                  
                   <?php 
				    
					  $projectImg = "img/ui-zac.jpg";
					  echo "<div class='col-md-3'><a data-toggle='tab' href='#contact'>AS Order</a></div>";
					  echo "<div class='col-md-3'><a data-toggle='tab' href='#contact'>TS Order</a></div>";
				  ?>
                    <!-- end custombox -->
                    
                  </div>
                  <!-- /tab-pane -->
                 
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>
                      
                        
                  </div>
                  <!-- end col-12 -->
                  
                  
                  
                  
               </div>
			
			
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start--><?php include('footer.php');?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/fancybox/jquery.fancybox.js"></script>
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
</script>
<!--script for this page-->
  <script type="text/javascript">
    $(function() {
      //    fancybox
      jQuery(".fancybox").fancybox();
    });
  </script>
</body>
<script>
var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myIndex++;
  if (myIndex > x.length) {myIndex = 1}    
  x[myIndex-1].style.display = "block";  
  setTimeout(carousel, 2000); // Change image every 2 seconds
}

</script>
</html>
