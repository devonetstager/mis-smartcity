<?php
include("dbconfig/dbconfig.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Smart City Thiruvananthapuram Ltd</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="lib/gritter/css/jquery.gritter.css" />
  <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
  <!-- Custom styles for this template -->
  <script src="lib/morris-conf.js"></script>
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <script src="lib/chart-master/Chart.js"></script>
  <style>#area-chart,
#line-chart,
#bar-chart,
#stacked,
#pie-chart{
  min-height: 250px;
}</style>

  <!-- =======================================================
    Template Name: Smart City Thiruvananthapuram Ltd
    Template URL: https://templatemag.com/Smart City Thiruvananthapuram Ltd-bootstrap-admin-template/
    
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <?php
	include("header_t.php");
	?>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <?php include("sidebar2.php")?>
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
		<?php
		
		$sqlSCP = "SELECT COUNT(DISTINCT P_id) AS count from allprojects WHERE p_type <> 'Convergence'";
		
		
		$sql = "SELECT * from allprojects WHERE p_type <> 'Convergence'";
		$sqlAmt = "SELECT SUM(p_estcost) AS total FROM project_details_with_fund";
		
		$sqlDPR = "SELECT sum( dpr_count) AS dcount FROM allprojects  WHERE p_type <> 'Convergence'";
		$sqlDPRsum = "SELECT sum(dpr_amt) AS total FROM allprojects";
		
		//$sqlDPR = "SELECT sum( as_count) AS dcount FROM allprojects WHERE p_type <> 'Convergence'";
		//$sqlDPRsum = "SELECT sum(as_amount) AS total  FROM `allprojects` WHERE p_type <> 'Convergence'";
		
		$sqlAS = "SELECT sum( as_count) AS ascount FROM allprojects WHERE p_type <> 'Convergence'";
		$sqlASsum = "SELECT sum(as_amt) AS total  FROM `allprojects`";
		
		$sqlTS = "SELECT sum( ts_count) AS tscount FROM allprojects WHERE p_type <> 'Convergence'";
		$sqlTSsum = "SELECT sum(ts_amt) AS total  FROM `allprojects`";
		
		$sqlTender = "SELECT sum( tender_count) AS tendercount FROM allprojects WHERE p_type <> 'Convergence'";
		$sqlTendersum = "SELECT sum(tender_amt) AS total  FROM `allprojects`";
		
		$sqlwa = "SELECT sum( wa_count) AS wacount FROM allprojects WHERE p_type <> 'Convergence'";
		$sqlwasum = "SELECT sum(wa_amt ) AS total FROM `allprojects`";
        
        $sqlwc = "SELECT sum( wc_count) AS wccount FROM allprojects WHERE p_type <> 'Convergence'";
		$sqlwcsum = "SELECT sum(wc_amt ) AS total FROM `allprojects`";
					

if ($resultSCP=mysqli_query($conn,$sqlSCP))
  {
  // Project module Count
  $rowcountscp=mysqli_fetch_assoc($resultSCP);
  mysqli_free_result($resultSCP);
  }
		
if ($result=mysqli_query($conn,$sql))
  {
  // Project Count
  $rowcount=mysqli_num_rows($result);
  mysqli_free_result($result);
  }

			

if ($result2=mysqli_query($conn,$sqlAmt))
  {
  $amount = mysqli_fetch_assoc($result2);
 // project total amount
  }
  
  if ($result3=mysqli_query($conn,$sqlDPR))
  {
  // DPR Count
  $rowcount3=mysqli_fetch_assoc($result3);
  mysqli_free_result($result3);
  }
  
  if ($result4=mysqli_query($conn,$sqlDPRsum))
  {
  $amount4 = mysqli_fetch_assoc($result4);
 // DPR total amount
  }
  
  
   if ($result5=mysqli_query($conn,$sqlAS))
  {
  // AS Count
  $rowcount5=mysqli_fetch_assoc($result5);
  mysqli_free_result($result5);
  }
  
  if ($result6=mysqli_query($conn,$sqlASsum))
  {
  $amount6 = mysqli_fetch_assoc($result6);
 // AS total amount
  }
  
  
  if ($result7=mysqli_query($conn,$sqlTS))
  {
  // TS Count
  $rowcount7=mysqli_fetch_assoc($result7);
  mysqli_free_result($result7);
  }
  
  if ($result8=mysqli_query($conn,$sqlTSsum))
  {
  $amount8 = mysqli_fetch_assoc($result8);
 //TS total amount
  }
  
  
  
  if ($result9=mysqli_query($conn,$sqlTender))
  {
  // Tender Count
  $rowcount9=mysqli_fetch_assoc($result9);
  mysqli_free_result($result9);
  }
  
  if ($result10=mysqli_query($conn,$sqlTendersum))
  {
  $amount10 = mysqli_fetch_assoc($result10);
 //Tender total amount
  }
  
  
  if ($result11=mysqli_query($conn,$sqlwa))
  {
  // Work awarded Count
  $rowcount11=mysqli_fetch_assoc($result11);
  mysqli_free_result($result11);
  }
  
  if ($result12=mysqli_query($conn,$sqlwasum))
  {
  $amount12 = mysqli_fetch_assoc($result12);
 //Work awarded total amount
  }
  
   if ($result13=mysqli_query($conn,$sqlwcsum))
  {
  $amount13 = mysqli_fetch_assoc($result13);
 //Work awarded total amount
  }
  
//mysqli_close($conn);			
			
			
			//$result->close();
			$totSCP_1920 = $rowcountscp['count'];
			
			$totnum_1920 = $rowcount;
			$totamt_1920 = round($amount['total'],2);
		
			$dprno_1920 = $rowcount3['dcount'];
			$dpramt_1920 = round($amount4['total'],2);
			
			$asno_1920 = $rowcount5['ascount'];
			$asamt_1920 = round($amount6['total'],2);
			
			$tsno_1920 = $rowcount7['tscount'];
			$tsamt_1920 = round($amount8['total'],2);
			
			$tdrno_1920 = $rowcount9['tendercount'];
			$tdramt_1920 = round($amount10['total'],2);
			
			$awdno_1920 = $rowcount11['wacount'];
			$awdamt_1920 = round($amount12['total'],2);
            
            $wcamt_1920 = round($amount13['total'],2);
		?>
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <div class="row content-panel mt mb">
        
        <?php
        include("home_first_row.php");
        ?>
        </div>
        <div class="col-lg-12 ">
			  <div class="border-head">
					  <br>
					  <br>
			  
                  
				</div>
        <div class="row content-panel mt mb">
        
        <?php
        include("morris.php");
        ?>
        </div>
          
                
                
              <!-- /col-md-4 -->
             
            </div>
            <!-- /row -->
			
			
			
          </div>
          <!-- /col-lg-9 END SECTION MIDDLE -->
          
      </section>
    </section>
    <!--main content end-->
    <?php include("footer.php")?>
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>

  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <script type="text/javascript" src="lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="lib/gritter-conf.js"></script>
  <!--script for this page-->
  
  <script src="lib/morris/morris.min.js"></script>
  <script src="lib/sparkline-chart.js"></script>
  <script src="lib/zabuto_calendar.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      var unique_id = $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Welcome Admin!',
        // (string | mandatory) the text inside the notification
        text: '',
        // (string | optional) the image to display on the left
        image: 'img/sctl_logo.png',
        // (bool | optional) if you want it to fade out on its own or just sit there
        sticky: false,
        // (int | optional) the time you want it to be alive for before fading out
        time: 8000,
        // (string | optional) the class name you want to apply to that specific message
        class_name: 'my-sticky-class'
      });

      return false;
    });
  </script>
  <script type="application/javascript">
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        },
        legend: [{
            type: "text",
            label: "Special event",
            badge: "00"
          },
          {
            type: "block",
            label: "Regular event",
          }
        ]
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>
  
  <script>var data = [
      { y: '2014', a: 50, b: 90},
      { y: '2015', a: 65,  b: 75},
      { y: '2016', a: 50,  b: 50},
      { y: '2017', a: 75,  b: 60},
      { y: '2018', a: 80,  b: 65},
      { y: '2019', a: 90,  b: 70},
      { y: '2020', a: 100, b: 75},
      { y: '2021', a: 115, b: 75},
      { y: '2022', a: 120, b: 85},
      { y: '2023', a: 145, b: 85},
      { y: '2024', a: 160, b: 95}
    ],
    config = {
      data: data,
      xkey: 'y',
      ykeys: ['a', 'b'],
      labels: ['Total Income', 'Total Outcome'],
      fillOpacity: 0.6,
      hideHover: 'auto',
      behaveLikeLine: true,
      resize: true,
      pointFillColors:['#ffffff'],
      pointStrokeColors: ['black'],
      lineColors:['gray','red']
  };
config.element = 'area-chart';
Morris.Area(config);
config.element = 'line-chart';
Morris.Line(config);
config.element = 'bar-chart';
Morris.Bar(config);
config.element = 'stacked';
config.stacked = true;
Morris.Bar(config);
Morris.Donut({
  element: 'pie-chart',
  data: [
    {label: "Friends", value: 30},
    {label: "Allies", value: 15},
    {label: "Enemies", value: 45},
    {label: "Neutral", value: 10}
  ]
});</script>
  
</body>

</html>
