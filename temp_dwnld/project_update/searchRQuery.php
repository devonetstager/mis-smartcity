<?php

/**
 * @author Anoop V
 * @copyright 2020
 */


				    $sql = "SELECT  * FROM    projectrestructured";
					$result = mysqli_query($conn, $sql);
					if (mysqli_num_rows($result) > 0) {
						// output data of each row
						while($row = mysqli_fetch_assoc($result)) {
						/**
                         *  DPR sum
                         */
                        $sqldprsum = "SELECT SUM(dpr_amount) AS dprSum FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsprsum = mysqli_query($conn, $sqldprsum);
						$rowdprsum = mysqli_fetch_assoc($resultsprsum);
                        if(round($rowdprsum["dprSum"],2)>0){ $rowdprsum1 = round($rowdprsum["dprSum"],2); }else $rowdprsum1 = "";
                        
                        /**
						 * 
						 */
                        /**
                         *  More details of projectss
                         */
                        $sqlprojDetail = "SELECT * FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultprojDetail = mysqli_query($conn, $sqlprojDetail);
						$rowprojDetail = mysqli_fetch_assoc($resultprojDetail);
                        
						/**
						 * 
						 */
                         /**
                         *  AS sum
                         */
                        $sqlASsum = "SELECT SUM(as_amount) AS ASSum FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsASsum = mysqli_query($conn, $sqlASsum);
						$rowASsum = mysqli_fetch_assoc($resultsASsum);
                        if(round($rowASsum["ASSum"],2)>0){ $rowASsum1 = round($rowASsum["ASSum"],2); }else $rowASsum1 = "";
                        
                        /**
						 * 
						 */
                         /**
                         *  TS sum
                         */
                        $sqlTSsum = "SELECT SUM(ts_amount) AS TSSum FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsTSsum = mysqli_query($conn, $sqlTSsum);
						$rowTSsum = mysqli_fetch_assoc($resultsTSsum);
                        if(round($rowTSsum["TSSum"],2)>0){ $rowTSsum1 = round($rowTSsum["TSSum"],2); }else $rowTSsum1 = "";
                        
                        /**
						 * 
						 */
                         /**
                         *  Tender sum
                         */
                        $sqlTendersum = "SELECT SUM(tender_amount) AS TenderSum FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsTendersum = mysqli_query($conn, $sqlTendersum);
						$rowTendersum = mysqli_fetch_assoc($resultsTendersum);
                        if(round($rowTendersum["TenderSum"],2)>0){ $rowTendersum1 = round($rowTendersum["TenderSum"],2); }else $rowTendersum1 = "";
                         
                        /**
						 * 
						 */
                         /**
                         *  Work awarded sum
                         */
                        $sqlWorkAwardsum = "SELECT SUM(wa_amount) AS workawardedSum FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsWorkAwardsum = mysqli_query($conn, $sqlWorkAwardsum);
						$rowWorkAwardsum = mysqli_fetch_assoc($resultsWorkAwardsum);
                        if(round($rowWorkAwardsum["workawardedSum"],2)>0){ $workawardedSum1 = round($rowWorkAwardsum["workawardedSum"],2); }else $workawardedSum1 = "";
                         
                        /**
						 * 
						 */
                         /**
                         *  Work Completion sum
                         */
                        $sqlworkCompletionSum = "SELECT SUM(wc_amount) AS workCompletionSum FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsCompletionSum = mysqli_query($conn, $sqlworkCompletionSum);
						$rowCompletionSum = mysqli_fetch_assoc($resultsCompletionSum);
                        if(round($rowCompletionSum["workCompletionSum"],2)>0){ $workCompletionSum1 = round($rowCompletionSum["workCompletionSum"],2); }else $workCompletionSum1 = "";
    
                        /**
						 * 
						 */
                         /**
                         *  Project Status
                         */
                        $sqlProjectStatus = "SELECT p_status FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsProjectStatus = mysqli_query($conn, $sqlProjectStatus);
						$rowProjectStatus = mysqli_fetch_assoc($resultsProjectStatus);
                        
                        /**
						 * 
						 */
                         /**
                         *  Project Remarks
                         */
                        $sqlProjectRemark = "SELECT p_remark FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsProjectRemark = mysqli_query($conn, $sqlProjectRemark);
						$rowProjectRemark = mysqli_fetch_assoc($resultsProjectRemark);
                        
                        /**
						 * Condition for update details
						 */
                         if($rowprojDetail["sub_pid"] == '') //Also tried this "if(strlen($strTemp) > 0)"
                            {
                                 $UpdateDet = "<a href='project_all_update.php?link=:".$rowprojDetail["p_id"]."'>Update</a>";
                            } else $UpdateDet =  "";
                        
                         /**
                          * 
                          */
                          /**
						 * Condition for More details
						 */
                         if($rowprojDetail["sub_pid"] == '') //Also tried this "if(strlen($strTemp) > 0)"
                            {
                                 $moreDet = "";
                            } else $moreDet =  ":-&nbsp;&nbsp;&nbsp; <div style='color:black;'>Click for Sub projects</div>";
                        
                         /**
                          * 
                          */

?>