<div class="form-group" style="border-bottom-color: #2f323a; margin-left: 0px ;margin-right: 0px;">
                <?php include("dprQuery.php");?>
                 <h4 style="text-align: left; margin-left: 15px; text-decoration: underline;"><i class="fa"></i> DPR Details</h4>
                <div class="row content-panel" style="margin-left: 0px;margin-right: 0px;">
              <div class="panel-heading">
                <ul class="nav nav-tabs nav-justified">
                  <li class="active">
                    <a data-toggle="tab" href="#dprcivil">Civil</a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#dprelec" class="contact-map">Electrical</a>
                  </li>   
                  <li>
                    <a data-toggle="tab" href="#dprit" class="contact-map">IT</a>
                  </li>    
                  <li>
                    <a data-toggle="tab" href="#dproth" class="contact-map">Other</a>
                  </li>              
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body">
                <div class="tab-content">
                  <div id="dprcivil" class="tab-pane">
                            
                             
                  <label class="col-lg-1 control-label">Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_civil_amt"  name="dpr_civil_amt" class="form-control" value="<?php  echo $dpr_civil_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_status"  name="dpr_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="dpr_civil_status" style="width:100%; height:35px">
					 <option value="<?php  echo $dpr_civil_status; ?>"><?php  echo $dpr_civil_status; ?></option>
                     <option value="">NIL</option>
                     <option value="Preparation">Preparation</option>
 					 <option value="Submitted">Submitted</option>
                     <option value="Revision">Revision</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Approved">Approved</option>
                     <option value="Rejected">Rejected</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">Document No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_civil_orderno"  name="dpr_civil_orderno" class="form-control" value="<?php  echo $dpr_civil_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Date of Submission </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_civil_sdate"  name="dpr_civil_date" class="form-control" value="<?php  echo $dpr_civil_date; ?>">
                  </div>
                  <!-- dpr File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                                     
                   <label class="col-lg-1 control-label"> Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exisdpr"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exisdpr" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- dpr File upload -->      
                  </div><!-- dpr details-->
                  
                  <!-- dpr Elec details-->
                  <!-- /tab-pane -->
                  <div id="dprelec" class="tab-pane">
                   
                  <label class="col-lg-1 control-label">Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_ele_amt"  name="dpr_ele_amt" class="form-control" value="<?php  echo $dpr_ele_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_status"  name="dpr_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="dpr_ele_status" style="width:100%; height:35px">
					 <option value="<?php  echo $dpr_ele_status; ?>"><?php  echo $dpr_ele_status; ?></option>
                     <option value="">NIL</option>
                     <option value="Preparation">Preparation</option>
 					 <option value="Submitted">Submitted</option>
                     <option value="Revision">Revision</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Approved">Approved</option>
                     <option value="Rejected">Rejected</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">Document No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_ele_orderno"  name="dpr_ele_orderno" class="form-control" value="<?php  echo $dpr_ele_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Date of Submission </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_ele_date"  name="dpr_ele_date" class="form-control" value="<?php  echo $dpr_ele_date; ?>">
                  </div>
                  
                  <!-- dpr File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                                     
                   <label class="col-lg-1 control-label">Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exisdpr"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exisdpr" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- dpr File upload -->
                   
                  </div>
                  <!-- /tab-pane -->
                  <!-- /tab-pane -->
                  <div id="dprit" class="tab-pane active">
                   
                   <label class="col-lg-1 control-label">Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_it_amt"  name="dpr_it_amt" class="form-control" value="<?php  echo $dpr_it_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_status"  name="dpr_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="dpr_it_status" style="width:100%; height:35px">
					 <option value="<?php  echo $dpr_it_status; ?>"><?php  echo $dpr_it_status; ?></option>
                     <option value="">NIL</option>
                     <option value="Preparation">Preparation</option>
 					 <option value="Submitted">Submitted</option>
                     <option value="Revision">Revision</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Approved">Approved</option>
                     <option value="Rejected">Rejected</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">Document No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_it_orderno"  name="dpr_it_orderno" class="form-control" value="<?php  echo $dpr_it_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Date of Submission</label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_it_date"  name="dpr_it_date" class="form-control" value="<?php  echo $dpr_it_date; ?>">
                  </div>
                  <!-- dpr File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   
                   
                   <label class="col-lg-1 control-label">Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exisdpr"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exisdpr" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- dpr File upload -->
                   
                  </div>
                  <!-- /tab-pane -->
                  <!-- /tab-pane -->
                  <div id="dproth" class="tab-pane">
                   
                  <label class="col-lg-1 control-label">Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_other_amt"  name="dpr_other_amt" class="form-control" value="<?php  echo $dpr_other_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_status"  name="dpr_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="dpr_other_status" style="width:100%; height:35px">
					 <option value="<?php  echo $dpr_other_status; ?>"><?php  echo $dpr_other_status; ?></option>
                     <option value="">NIL</option>
                     <option value="Preparation">Preparation</option>
 					 <option value="Submitted">Submitted</option>
                     <option value="Revision">Revision</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Approved">Approved</option>
                     <option value="Rejected">Rejected</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">Document No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_other_orderno"  name="dpr_other_orderno" class="form-control" value="<?php  echo $dpr_other_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Date of Submission</label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="dpr_other_date"  name="dpr_other_date" class="form-control" value="<?php  echo $dpr_other_date; ?>">
                  </div>
                  <!-- dpr File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   
                   <label class="col-lg-1 control-label">Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exisdpr"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exisdpr" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- dpr File upload -->
                   
                  </div>
                  <!-- /tab-pane -->
                 
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
                  </div>