<?php
include("dbconfig/dbconfig.php")
?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>SCTL - Search Projects</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="lib/advanced-datatable/css/DT_bootstrap.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    
  ======================================================= -->
</head>

<body  onload="zoom()">
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index_new_dash.php" class="logo"><b>Smart City Thiruvananthapuram Ltd</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <!--  <li><a class="logout" href="logout.php">Logout</a></li>-->
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
		<?php //$userName = $_SESSION["name"];
        $userName = "SCTL"?>
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="#"><img src="img/sctl_logo.png" class="img-circle" width="80"></a></p>
          <h5 class="centered"><?php echo "User"; ?></h5>
          <li class="mt">
            <a href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
		  <li class="mt">
            <a href="projectRsearchAll.php" class="active">
              <i class="fa fa-dashboard"></i>
              <span>All Projects</span>
              </a>
          </li>
		  
		  
         
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> List of Complete SCP Projects (including Convergence)</h3>
        <div class="row mb">
          <!-- page start-->
          <div class="content-panel">
            <div class="adv-table">
             <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- <table class="table table-striped reportTable">
 -->
 <form class="form-horizontal style-form" method="POST" action="projectRUpdateQuery.php">
 <table cellpadding="0" cellspacing="0" border="0" class=" table table-bordered" style="background-color: #8090de;" >
    <thead>
        <tr>
                    <th style="text-align: center;width: 10px;">ID</th>
                    <th style="text-align: center;width: 150px;">Project Name</th>
                    <th class="hidden-phone" style="text-align: center;">SAR</th>
                    <th class="hidden-phone" style="text-align: center;">FR</th>
                    <th class="hidden-phone" style="text-align: center;">DPR Amt<br><div style="font-size: smaller;">(in Cr)</div></th>
                    <th class="hidden-phone" style="text-align: center;">AS Amt<br><div style="font-size: smaller;">(in Cr)</div></th>
					<th class="hidden-phone" style="text-align: center;">TS Amt<br><div style="font-size: smaller;">(in Cr)</div></th>
                    <th class="hidden-phone" style="text-align: center;">Tender Amt<br><div style="font-size: smaller;">(in Cr)</div></th>
					<th class="hidden-phone" style="text-align: center;">Work Awarded Amt<br><div style="font-size: smaller;">(in Cr)</div></th>
					<th class="hidden-phone" style="text-align: center;">Work Completed Amt<br><div style="font-size: smaller;">(in Cr)</div></th>
                    <th class="hidden-phone" style="text-align: center;">Physical Work Progress<br><div style="font-size: smaller;">(in %)</div></th>
                    <th class="hidden-phone" style="text-align: center;">Expenditure Progress<br><div style="font-size: smaller;">(in %)</div></th>
                    <th class="hidden-phone" style="text-align: center;">Present Status</th>
                  
    </thead>
    <tbody>
    
    <?php
				    $sql = "SELECT DISTINCT p_id FROM allprojects ORDER BY p_id REGEXP '^[A-Z]{2}' ASC, IF(p_id REGEXP '^[A-Z]{2}', LEFT(p_id, 2), LEFT(p_id, 1)),
           CAST(IF(p_id REGEXP '^[A-Z]{2}', RIGHT(p_id, LENGTH(p_id) - 2), RIGHT(p_id, LENGTH(p_id) - 1)) AS SIGNED)";
					$result = mysqli_query($conn, $sql);
					if (mysqli_num_rows($result) > 0) {
						// output data of each row
						while($row = mysqli_fetch_assoc($result)) {
						/**
                         *  DPR sum
                         */
                        $sqldprsum = "SELECT SUM(dpr_amt) AS dprSum FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsprsum = mysqli_query($conn, $sqldprsum);
						$rowdprsum = mysqli_fetch_assoc($resultsprsum);
                        if(round($rowdprsum["dprSum"],2)>0)
                        { $rowdprsum1 = round($rowdprsum["dprSum"],2);
                         $colordpr ="limegreen"; }
                        else 
                        {$rowdprsum1 = "";
                        $colordpr ="#ccc7c7";
                        }
                        
                        /**
						 * 
						 */
                        /**
                         *  More details of projectss
                         */
                        $sqlprojDetail = "SELECT * FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultprojDetail = mysqli_query($conn, $sqlprojDetail);
						$rowprojDetail = mysqli_fetch_assoc($resultprojDetail);
                        
						/**
						 * 
						 */
                         /**
                         *  AS sum
                         */
                        $sqlASsum = "SELECT SUM(as_amt) AS ASSum FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsASsum = mysqli_query($conn, $sqlASsum);
						$rowASsum = mysqli_fetch_assoc($resultsASsum);
                        if(round($rowASsum["ASSum"],2)>0){ $rowASsum1 = round($rowASsum["ASSum"],2); }else $rowASsum1 = "";
                        
                        /**
						 * 
						 */
                         /**
                         *  TS sum
                         */
                        $sqlTSsum = "SELECT SUM(ts_amt) AS TSSum FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsTSsum = mysqli_query($conn, $sqlTSsum);
						$rowTSsum = mysqli_fetch_assoc($resultsTSsum);
                        if(round($rowTSsum["TSSum"],2)>0){ $rowTSsum1 = round($rowTSsum["TSSum"],2); }else $rowTSsum1 = "";
                        
                        /**
						 * 
						 */
                         /**
                         *  Tender sum
                         */
                        $sqlTendersum = "SELECT SUM(tender_amt) AS TenderSum FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsTendersum = mysqli_query($conn, $sqlTendersum);
						$rowTendersum = mysqli_fetch_assoc($resultsTendersum);
                        if(round($rowTendersum["TenderSum"],2)>0){ $rowTendersum1 = round($rowTendersum["TenderSum"],2); }else $rowTendersum1 = "";
                         
                        /**
						 * 
						 */
                         /**
                         *  Work awarded sum
                         */
                        $sqlWorkAwardsum = "SELECT SUM(wa_amt) AS workawardedSum FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsWorkAwardsum = mysqli_query($conn, $sqlWorkAwardsum);
						$rowWorkAwardsum = mysqli_fetch_assoc($resultsWorkAwardsum);
                        if(round($rowWorkAwardsum["workawardedSum"],2)>0){ $workawardedSum1 = round($rowWorkAwardsum["workawardedSum"],2); }else $workawardedSum1 = "";
                         
                        /**
						 * 
						 */
                         /**
                         *  Work Completion sum
                         */
                        $sqlworkCompletionSum = "SELECT SUM(wc_amt) AS workCompletionSum FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsCompletionSum = mysqli_query($conn, $sqlworkCompletionSum);
						$rowCompletionSum = mysqli_fetch_assoc($resultsCompletionSum);
                        if(round($rowCompletionSum["workCompletionSum"],2)>0){ $workCompletionSum1 = round($rowCompletionSum["workCompletionSum"],2); }else $workCompletionSum1 = "";
    
                        /**
						 * 
						 */
                         /**
                         *  Project Status
                         */
                        $sqlProjectStatus = "SELECT p_status FROM allprojects WHERE p_id='".$row["p_id"]."'";
						$resultsProjectStatus = mysqli_query($conn, $sqlProjectStatus);
						$rowProjectStatus = mysqli_fetch_assoc($resultsProjectStatus);
                        
                        /**
						 * 
						
                        
                        /**
						 * Condition for update details
						 */
                         if($rowprojDetail["sub_pid"] == '') //Also tried this "if(strlen($strTemp) > 0)"
                            {
                                 $UpdateDet = "<a href='project_all_update.php?link=:".$rowprojDetail["p_id"]."'>Update</a>";
                            } else $UpdateDet =  "";
                        
                         /**
                          * 
                          */
                          /**
						 * Condition for More details
						 */
                         if($rowprojDetail["sub_pid"] == '') //Also tried this "if(strlen($strTemp) > 0)"
                            {
                                 $moreDet = "";
                            } else $moreDet =  ":-&nbsp;&nbsp;&nbsp; <div style='color:black;'>Click for Sub projects</div>";
                        
                         /**
                          * 
                          */
                         
        echo "<tr style='background-color:#ccc7c7'>
            <td style='width : 10px'><a class='showhr' href='#'>".$row["p_id"]."</td>
            <td style='width : 150px'><b><a class='showhr' href='#'>".$rowprojDetail["p_name"]."</b>".$moreDet."</td>
            <td style='width : 70px'><input type='text' class='form-control' placeholder='' value='&#10004;'></td>
            <td style='width : 70px'><input type='text' class='form-control' placeholder='' value='&#10004;'></td>
            <td style='width : 70px; background-color: ".$colordpr."';><input type='text' class='form-control' placeholder='' value=".$rowdprsum1."></td>
            <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowASsum1."></td>
            <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowTSsum1."></td>
            <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowTendersum1."></td>
            <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$workawardedSum1."></td>
            <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$workCompletionSum1."></td>
            <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowprojDetail["pw_prog"]."></td>
            <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowprojDetail["exp_prog"]."></td>
            <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowprojDetail["p_status"]."></td>
            
            
            
            
        </tr>";
        $workCompletionSum1 = 0;
        $sqlsub = "SELECT * FROM allprojects WHERE p_id='".$row["p_id"]."'";
		$resultsub = mysqli_query($conn, $sqlsub);

					if (mysqli_num_rows($resultsub) > 0) {
						// output data of each row
						while($rowsub = mysqli_fetch_assoc($resultsub)) {
                        if($rowsub["sub_pid"]){		  
                         echo "<tr class='aser' style='background-color:rgb(241, 241, 241)'>
                                    <!--child row-->
                                    <td style='width : 10px'>".$rowsub["sub_pid"]."</td>
                                    <td style='width : 150px'>".$rowsub["sub_pname"]."</td>
                                    <td style='width : 70px'><input type='text' class='form-control' placeholder='' value='&#10004;'></td>
                                    <td style='width : 70px'><input type='text' class='form-control' placeholder='' value='&#10004;'></td>
                                    <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowsub["dpr_amt"]."></td>
                                    <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowsub["as_amt"]."></td>
                                    <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowsub["ts_amt"]."></td>
                                    <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowsub["tender_amt"]."></td>
                                    <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowsub["wa_amt"]."></td>
                                    <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowsub["wc_amt"]."></td>
                                    <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowsub["pw_prog"]."></td>
                                    <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowsub["exp_prog"]."></td>
                                    <td style='width : 70px'><input type='text' class='form-control' placeholder='' value=".$rowsub["p_status"]."></td>
                                                                      
                                    
                                </tr>";}  
                                                                        }
                                                        }?>
        
        
        <?php 
        	}
						
					} else {
						echo "0 results";
					}
					
					
					$conn->close();
				  
				  
				  
				  ?>
                  <button type="submit" class="btn btn-theme">Update</button>
                  </form>
    </tbody>
</table>
            </div>
            
          </div>
          <!-- page end-->
        </div>
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php")?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  
  
  
  
  <script type="text/javascript">
  $(".aser").show()
  $(".showhr").click(function() {
    event.preventDefault();
    $(this).closest('tr').nextUntil("tr:has(.showhr)").toggle("slow", function() {});
});
  
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
        
      if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        //xmlhttp.open("GET","getuser.php?q="+str,true);
        xmlhttp.open("GET","getuser.php",true);
        xmlhttp.send();
    }
      
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
      nCloneTd.innerHTML = '<img src="lib/advanced-datatable/images/details_open.png">';
      nCloneTd.className = "center";

      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "lib/advanced-datatable/media/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "lib/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
  <script type="text/javascript">
        function zoom() {
            document.body.style.zoom = "90%" 
        }
</script>
</body>

</html>
