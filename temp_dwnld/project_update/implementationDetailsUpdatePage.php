<div class="form-group" style="border-bottom-color: #2f323a; margin-left: 0px ;margin-right: 0px;">
                <?php 
                            include("execQuery.php");
                            ?>
                 <h4 style="text-align: left; margin-left: 15px; text-decoration: underline;"><i class="fa"></i> Implementation Details</h4>
                <div class="row content-panel" style="margin-left: 0px;margin-right: 0px;">
              <div class="panel-heading">
                <ul class="nav nav-tabs nav-justified">
                  <li class="active">
                    <a data-toggle="tab" href="#wacivil">Civil</a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#waelec" class="contact-map">Electrical</a>
                  </li>   
                  <li>
                    <a data-toggle="tab" href="#wait" class="contact-map">IT</a>
                  </li>    
                  <li>
                    <a data-toggle="tab" href="#waoth" class="contact-map">Other</a>
                  </li>              
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body">
                <div class="tab-content">
                  <div id="wacivil" class="tab-pane active">
                            
                             
                  <label class="col-lg-1 control-label">Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_civil_amt"  name="exec_civil_amt" class="form-control" value="<?php  echo $exec_civil_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label"> Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_status"  name="exec_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="exec_civil_status" style="width:100%; height:35px">
					 <option value="<?php  echo $exec_civil_status; ?>"><?php  echo $exec_civil_status; ?></option>
                     <option value="">NIL</option>
                     <option value="On Hold">On Hold</option>
 					 <option value="LOA Issued">LOA Issued</option> 
                      <option value="Work In Progress">Work In Progress</option>
                      <option value="Work Completed">Work Completed</option> 					 
				     <option value="Completion Certificate Issued">Completion Certificate Issued</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_civil_orderno"  name="exec_civil_orderno" class="form-control" value="<?php  echo $exec_civil_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Start Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_civil_sdate"  name="exec_civil_sdate" class="form-control" value="<?php  echo $exec_civil_sdate; ?>">
                  </div>
                  <!-- exec File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   <label class="col-lg-1 control-label">End Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_civil_edate"  name="exec_civil_edate" class="form-control" value="<?php  echo $exec_civil_edate; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Agency </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_civil_agency"  name="exec_civil_agency" class="form-control" value="<?php  echo $exec_civil_agency; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exisexec"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exisexec" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- exec File upload -->      
                  </div><!-- exec details-->
                  
                  <!-- exec Elec details-->
                  <!-- /tab-pane -->
                  <div id="waelec" class="tab-pane">
                   
                  <label class="col-lg-1 control-label">Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_ele_amt"  name="exec_ele_amt" class="form-control" value="<?php  echo $exec_ele_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_status"  name="exec_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="exec_ele_status" style="width:100%; height:35px">
					 <option value="<?php  echo $exec_ele_status; ?>"><?php  echo $exec_ele_status; ?></option>
                     <option value="">NIL</option>
                     <option value="On Hold">On Hold</option>
 					 <option value="LOA Issued">LOA Issued</option> 
                      <option value="Work In Progress">Work In Progress</option>
                      <option value="Work Completed">Work Completed</option> 					 
				     <option value="Completion Certificate Issued">Completion Certificate Issued</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_ele_orderno"  name="exec_ele_orderno" class="form-control" value="<?php  echo $exec_ele_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Start Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_ele_sdate"  name="exec_ele_sdate" class="form-control" value="<?php  echo $exec_ele_sdate; ?>">
                  </div>
                  
                  <!-- exec File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   <label class="col-lg-1 control-label">End Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_ele_edate"  name="exec_ele_edate" class="form-control" value="<?php  echo $exec_ele_edate; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Agency </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_ele_agency"  name="exec_ele_agency" class="form-control" value="<?php  echo $exec_ele_agency; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exisexec"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exisexec" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- exec File upload -->
                   
                  </div>
                  <!-- /tab-pane -->
                  <!-- /tab-pane -->
                  <div id="wait" class="tab-pane">
                   
                   <label class="col-lg-1 control-label">Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_it_amt"  name="exec_it_amt" class="form-control" value="<?php  echo $exec_it_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_status"  name="exec_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="exec_it_status" style="width:100%; height:35px">
					 <option value="<?php  echo $exec_it_status; ?>"><?php  echo $exec_it_status; ?></option>
                     <option value="">NIL</option>
                     <option value="On Hold">On Hold</option>
 					 <option value="LOA Issued">LOA Issued</option> 
                      <option value="Work In Progress">Work In Progress</option>
                      <option value="Work Completed">Work Completed</option> 					 
				     <option value="Completion Certificate Issued">Completion Certificate Issued</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_it_orderno"  name="exec_it_orderno" class="form-control" value="<?php  echo $exec_it_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">exec Start Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_it_sdate"  name="exec_it_sdate" class="form-control" value="<?php  echo $exec_it_sdate; ?>">
                  </div>
                  <!-- exec File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   
                   <label class="col-lg-1 control-label">End Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_it_edate"  name="exec_it_edate" class="form-control" value="<?php  echo $exec_it_edate; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Agency </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_it_agency"  name="exec_it_agency" class="form-control" value="<?php  echo $exec_it_agency; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exisexec"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exisexec" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- exec File upload -->
                   
                  </div>
                  <!-- /tab-pane -->
                  <!-- /tab-pane -->
                  <div id="waoth" class="tab-pane">
                   
                  <label class="col-lg-1 control-label">Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_other_amt"  name="exec_other_amt" class="form-control" value="<?php  echo $exec_other_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_status"  name="exec_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="exec_other_status" style="width:100%; height:35px">
					 <option value="<?php  echo $exec_other_status; ?>"><?php  echo $exec_other_status; ?></option>
                     <option value="">NIL</option>
                     <option value="On Hold">On Hold</option>
 					 <option value="LOA Issued">LOA Issued</option> 
                      <option value="Work In Progress">Work In Progress</option>
                      <option value="Work Completed">Work Completed</option> 					 
				     <option value="Completion Certificate Issued">Completion Certificate Issued</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_other_orderno"  name="exec_other_orderno" class="form-control" value="<?php  echo $exec_other_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Start Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_other_sdate"  name="exec_other_sdate" class="form-control" value="<?php  echo $exec_other_sdate; ?>">
                  </div>
                  <!-- exec File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   <label class="col-lg-1 control-label">End Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_other_edate"  name="exec_other_edate" class="form-control" value="<?php  echo $exec_other_edate; ?>">
                  </div>
                                      
                  <label class="col-lg-1 control-label">Agency </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exec_other_agency"  name="exec_other_agency" class="form-control" value="<?php  echo $exec_other_agency; ?>">
                  </div>
                   <label class="col-lg-1 control-label">Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exisexec"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exisexec" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- exec File upload -->
                   
                  </div>
                  <!-- /tab-pane -->
                 
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
                 </div>