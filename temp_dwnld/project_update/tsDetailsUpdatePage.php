<?php include("tsQuery.php");?>
                <div class="form-group" style="border-bottom-color: #2f323a; margin-left: 0px ;margin-right: 0px;">
                
                 <h4 style="text-align: left; margin-left: 15px; text-decoration: underline;"><i class="fa"></i> Technical Sanction Details</h4>
                <div class="row content-panel" style="margin-left: 0px;margin-right: 0px;">
                      <div class="panel-heading">
                        <ul class="nav nav-tabs nav-justified">
                          <li class="active" aria-expanded="true">
                            <a data-toggle="tab" aria-expanded="true" href="#civil">Civil</a>
                          </li>
                          <li>
                            <a data-toggle="tab" href="#elec" class="contact-map">Electrical</a>
                          </li>   
                          <li>
                            <a data-toggle="tab" href="#it" class="contact-map">IT</a>
                          </li>    
                          <li>
                            <a data-toggle="tab" href="#oth" class="contact-map">Other</a>
                          </li>              
                        </ul>
                      </div>
              <!-- /panel-heading -->
              <div class="panel-body">
                <div class="tab-content">
                  <div id="civil" class="tab-pane active">
                            
                             
                  <label class="col-lg-1 control-label">TS Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_civil_amt"  name="ts_civil_amt" class="form-control" value="<?php  echo $ts_civil_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">TS Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_status"  name="ts_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="ts_civil_status" style="width:100%; height:35px">
					 <option value="<?php  echo $ts_civil_status; ?>"><?php  echo $ts_civil_status; ?></option>
                     <option value="">NIL</option>
 					 <option value="Submitted">Submitted</option>
                     <option value="Revision">Revision</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Issued">Issued</option>
                     <option value="Rejected">Rejected</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">TS Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_civil_orderno"  name="ts_civil_orderno" class="form-control" value="<?php  echo $ts_civil_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">TS Issued Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_civil_date"  name="ts_civil_date" class="form-control" value="<?php  echo $ts_civil_date; ?>">
                  </div>
                  <!-- TS File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   
                   
                   <label class="col-lg-1 control-label">TS Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- TS File upload -->      
                  </div><!-- TS details-->
                  
                  <!-- TS Elec details-->
                  <!-- /tab-pane -->
                  <div id="elec" class="tab-pane">
                   
                  <label class="col-lg-1 control-label">TS Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_ele_amt"  name="ts_ele_amt" class="form-control" value="<?php  echo $ts_ele_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">TS Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_status"  name="ts_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="ts_ele_status" style="width:100%; height:35px">
					 <option value="<?php  echo $ts_ele_status; ?>"><?php  echo $ts_ele_status; ?></option>
                     <option value="">NIL</option>
 					 <option value="Submitted">Submitted</option>
                     <option value="Revision">Revision</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Issued">Issued</option>
                     <option value="Rejected">Rejected</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">TS Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_ele_orderno"  name="ts_ele_orderno" class="form-control" value="<?php  echo $ts_ele_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">TS Issued Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_ele_date"  name="ts_ele_date" class="form-control" value="<?php  echo $ts_ele_date; ?>">
                  </div>
                  <!-- TS File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   
                   
                   <label class="col-lg-1 control-label">TS Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- TS File upload -->
                   
                  </div>
                  <!-- /tab-pane -->
                  <!-- /tab-pane -->
                  <div id="it" class="tab-pane">
                   
                   <label class="col-lg-1 control-label">TS Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_it_amt"  name="ts_it_amt" class="form-control" value="<?php  echo $ts_it_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">TS Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_status"  name="ts_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="ts_it_status" style="width:100%; height:35px">
					 <option value="<?php  echo $ts_it_status; ?>"><?php  echo $ts_it_status; ?></option>
                     <option value="">NIL</option>
 					 <option value="Submitted">Submitted</option>
                     <option value="Revision">Revision</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Issued">Issued</option>
                     <option value="Rejected">Rejected</option>n>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">TS Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_it_orderno"  name="ts_it_orderno" class="form-control" value="<?php  echo $ts_it_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">TS Issued Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_it_date"  name="ts_it_date" class="form-control" value="<?php  echo $ts_it_date; ?>">
                  </div>
                  <!-- TS File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   
                   
                   <label class="col-lg-1 control-label">TS Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- TS File upload -->
                   
                  </div>
                  <!-- /tab-pane -->
                  <!-- /tab-pane -->
                  <div id="oth" class="tab-pane">
                   
                  <label class="col-lg-1 control-label">TS Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_other_amt"  name="ts_other_amt" class="form-control" value="<?php  echo $ts_other_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">TS Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_status"  name="ts_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="ts_other_status" style="width:100%; height:35px">
					 <option value="<?php  echo $ts_other_status; ?>"><?php  echo $ts_other_status; ?></option>
                     <option value="">NIL</option>
 					 <option value="Submitted">Submitted</option>
                     <option value="Revision">Revision</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Issued">Issued</option>
                     <option value="Rejected">Rejected</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">TS Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_other_orderno"  name="ts_other_orderno" class="form-control" value="<?php  echo $ts_other_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">TS Issued Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="ts_other_date"  name="ts_other_date" class="form-control" value="<?php  echo $ts_other_date; ?>">
                  </div>
                  <!-- TS File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   
                   
                   <label class="col-lg-1 control-label">TS Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- TS File upload -->
                   
                  </div>
                  <!-- /tab-pane -->
                 
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
                 </div>