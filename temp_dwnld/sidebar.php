<!--sidebar start-->
    <aside>
    <?php $userName = $_SESSION["name"]; ?>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="#"><img src="img/sctl_logo.png" class="img-circle" width="80"></a></p>
           <h5 class="centered"><?php echo $userName; ?></h5>
          <li class="mt">
            <a class="active" href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
		           
		   <li class="mt">
            <a href="searchAll.php">
              <i class="fa fa-dashboard"></i>
              <span>All Projects</span>
              </a>
          </li>
		  
		  <li class="mt">
            <a href="searchOngoing.php">
              <i class="fa fa-dashboard"></i>
              <span>Search Projects</span>
              </a>
          </li>
		  <li class="mt">
            <a href="scp_dpr_completed.php">
              <i class="fa fa-dashboard"></i>
              <span>DPR Completed Projects</span>
              </a>
          </li>
          <li class="mt">
            <a href="imageUpload.php" >
              <i class="fa fa-dashboard"></i>
              <span>Image Upload</span>
              </a>
          </li>
		 
         
          
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->