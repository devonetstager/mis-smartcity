<?php

/**
 * @author anoop
 * @copyright 2019
 */



?>


              <div class="col-md-6">
              <!-- /col-md-4 -->
     
              <div class="right-divider profile-text">
               
              <h3><i class="fa fa-angle-right"></i> Projects- Current Financial Year (2019-2020)</h3>
                
                
                <table class="table">
					<thead>
					<tr style="text-align:center; background-color:#999999; color:#FFFFFF">
						<th style="text-align:center">Description</th>
						
						<th style="text-align:center">Amount <br> (in Cr) </th>
                        <th style="text-align:center">% SCP Cost</th>
                        <th style="text-align:center">% AS Amount</th>
					   </tr>
					   </thead>
					   <tbody>
                       <tr>
						<td style="font-weight: bold;">Total SCP Modules: 43</td>
						<td style="font-weight: bold;">  1538.2</td>
						<td></td>
					  </tr>
                      <tr>
						<td><a href="dprCprojects.php"> Detailed Project Report</a></td>
					
						<td> <?php echo $dpramt_1920?> </td>
                        <td>
                                <div class="progress progress-striped active">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($dpramt_1920/1538.2)*100,2)."%";?>">
                                <span style="color: black;"><?php echo round(($dpramt_1920/1538.2)*100,2)."%";?></span>
                              </div>
                              </div>
                        </td>
                        <td>
                                
                        </td>
					  </tr>
					    <tr>
						<td><a href="asCprojects.php">A/s Accorded</a></td>
						
						<td> <?php echo $asamt_1920;?></td>
                        <td>
                                <div class="progress progress-striped active">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($asamt_1920/1538.2)*100,2)."%";?>">
                                <span style="color: black;"><?php echo round(($asamt_1920/1538.2)*100,2)."%";?></span>
                              </div>
                              </div>
                        </td>
                        <td>
                        
                        </td>
					  </tr>
					  					  
					  <tr>
						<td><a href="tsCprojects.php">T/s Issued</a></td>
						
						<td> <?php echo $tsamt_1920;?></td>
                        <td>
                                <div class="progress progress-striped active">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($tsamt_1920/1538.2)*100,2)."%";?>">
                                <span style="color: black;"><?php echo round(($tsamt_1920/1538.2)*100,2)."%";?></span>
                              </div>
                              </div>
                        </td>
                        <td>
                            <div class="progress progress-striped active">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($tsamt_1920/$asamt_1920)*100,2)."%";?>">
                                <span style="color: black;"><?php echo round(($tsamt_1920/$asamt_1920)*100,2)."%";?></span>
                              </div>
                              </div>
                        </td>
					  </tr>
					  <tr>
						<td><a href="tenderCprojects.php">Tendered</a></td>
						
						<td> <?php echo $tdramt_1920;?></td>
                        <td>
                                <div class="progress progress-striped active">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($tdramt_1920/1538.2)*100,2)."%";?>">
                                <span style="color: black;"><?php echo round(($tdramt_1920/1538.2)*100,2)."%";?></span>
                              </div>
                              </div>
                        </td>
                        <td>
                            <div class="progress progress-striped active">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($tdramt_1920/$asamt_1920)*100,2)."%";?>">
                                <span style="color: black;"><?php echo round(($tdramt_1920/$asamt_1920)*100,2)."%";?></span>
                              </div>
                              </div>
                        </td>
					  </tr>
					  <tr>
						<td><a href="waCprojects.php">Work awarded</a></td>
						
						<td> <?php echo $awdamt_1920;?></td>
                        <td>
                                <div class="progress progress-striped active">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($awdamt_1920/1538.2)*100,2)."%";?>">
                                <span style="color: black;"><?php echo round(($awdamt_1920/1538.2)*100,2)."%";?></span>
                              </div>
                              </div>
                        </td>
                        <td>
                            <div class="progress progress-striped active">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($awdamt_1920/$asamt_1920)*100,2)."%";?>">
                                <span style="color: black;"><?php echo round(($awdamt_1920/$asamt_1920)*100,2)."%";?></span>
                              </div>
                              </div>
                        </td>
					  </tr>
                      <tr>
						<td><a href="wcCprojects.php">Work Completed</a></td>
						
						<td> <?php echo $wcamt_1920;?></td>
                        <td>
                                <div class="progress progress-striped active">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($wcamt_1920/1538.2)*100,2)."%";?>">
                                <span style="color: black;"><?php echo round(($wcamt_1920/1538.2)*100,2)."%";?></span>
                              </div>
                              </div>
                        </td>
                        <td>
                            <div class="progress progress-striped active">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($wcamt_1920/$asamt_1920)*100,2)."%";?>">
                                <span style="color: black;"><?php echo round(($wcamt_1920/$asamt_1920)*100,2)."%";?></span>
                              </div>
                              </div>
                        </td>
					  </tr>
                      <tr>
						<td><a href="expCprojects.php">Expenditure</a></td>
						
						<td> <?php echo $wcamt_1920;?></td>
                        <td>
                                <div class="progress progress-striped active">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($wcamt_1920/1538.2)*100,2)."%";?>">
                                <span style="color: black;"><?php echo round(($wcamt_1920/1538.2)*100,2)."%";?></span>
                              </div>
                              </div>
                        </td>
                        <td>
                            <div class="progress progress-striped active">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($wcamt_1920/$asamt_1920)*100,2)."%";?>">
                                <span style="color: black;"><?php echo round(($wcamt_1920/$asamt_1920)*100,2)."%";?></span>
                              </div>
                              </div>
                        </td>
					  </tr>
					</tbody>
					
              </table>
                <br>
                
              </div>
              </div>
              <div class="col-md-6" style="height: 590px ;">
              <!-- /col-md-4 -->
               <iframe src="https://www.google.com/maps/d/embed?mid=1WMH55l9SmkmL-IDVgbFuZCJ-I6jQD9tB&hl=en" width="100%" height="-webkit-fill-available" style="
    height: inherit;
"></iframe>
            </div>
            
            <!-- /row -->
             
             
      