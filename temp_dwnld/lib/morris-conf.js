Morris.Area({
        element: 'hero-area',
        data: [
           {"period": "2018 Q1 April", "Admin Sanction": 0, "Technical Sanction": 0, "Tender Issued": 0, "Work Order Issued": 0},
           {"period": "2018 Q2 May", "Admin Sanction": 0, "Technical Sanction": 0, "Tender Issued": 0, "Work Order Issued": 0},
           {"period": "2018 Q3 June", "Admin Sanction": 0, "Technical Sanction": 0, "Tender Issued": 0, "Work Order Issued": 0},
           {"period": "2018 Q4 July", "Admin Sanction": 0, "Technical Sanction": 0, "Tender Issued": 0, "Work Order Issued": 0},
           {"period": "2018 Q1 Aug", "Admin Sanction": 0, "Technical Sanction": 0, "Tender Issued": 0, "Work Order Issued": 0},
           {"period": "2018 Q2 Sep", "Admin Sanction": 0, "Technical Sanction": 0, "Tender Issued": 0, "Work Order Issued": 0},
           {"period": "2018 Q3 Oct", "Admin Sanction": 1.02, "Technical Sanction": 1.02, "Tender Issued": 0, "Work Order Issued": 0},
           {"period": "2018 Q4 Nov", "Admin Sanction": 1.02, "Technical Sanction": 1.02, "Tender Issued": 0.40, "Work Order Issued": 0},
           {"period": "2018 Q1 Dec", "Admin Sanction": 1.02, "Technical Sanction": 1.02, "Tender Issued": 0.96, "Work Order Issued": 0},
           {"period": "2019 Q2 Jan", "Admin Sanction": 3.88, "Technical Sanction": 3.88, "Tender Issued": 3.13, "Work Order Issued": 0},
           {"period": "2019 Q3 Feb", "Admin Sanction": 46.21, "Technical Sanction": 5.30, "Tender Issued": 5.17, "Work Order Issued": 0.87},
           {"period": "2019 Q4 March", "Admin Sanction": 67.60, "Technical Sanction": 8.23, "Tender Issued": 6.30, "Work Order Issued": 3.63},
           {"period": "2019 Q1 April", "Admin Sanction": 67.60, "Technical Sanction": 8.23, "Tender Issued": 6.30, "Work Order Issued": 3.63}
        ],

          xkey: 'period',
          ykeys: ['Technical Sanction', 'Admin Sanction','Tender Issued','Work Order Issued'],
        
          hideHover: 'auto',
          lineWidth: 5,
          pointSize: 5,
          labels: ['Technical Sanction', 'Admin Sanction','Tender Issued','Work Order Issued'],
        lineColors:['#4ECDC4','#ed5565','#0affff','#ff0a0a'],
          fillOpacity: 0.5,
          smooth: true
      });
