<?php

include("dbconfig/dbconfig.php")

?>

<!DOCTYPE html>

<html lang="en">



<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="">

  <meta name="author" content="Dashboard">

  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

  <title>SCTL - Search Projects</title>



  <!-- Favicons -->

  <link href="img/favicon.png" rel="icon">

  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">



  <!-- Bootstrap core CSS -->

  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!--external css-->

  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />

  <link href="lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />

  <link href="lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />

  <link rel="stylesheet" href="lib/advanced-datatable/css/DT_bootstrap.css" />

  <!-- Custom styles for this template -->

  <link href="css/style.css" rel="stylesheet">

  <link href="css/style-responsive.css" rel="stylesheet">



  <!-- =======================================================

    

  ======================================================= -->
<script type="text/javascript">

function loaddata()
{
 var name=document.getElementById( "username" ).value;
	
 if(name)
 {
  $.ajax({
  type: 'post',
  url: 'loaddata.php',
  data: {
   user_name:name,
  },
  success: function (response) {
   // We get the element having id of display_info and put the response inside it
   $( '#display_info' ).html(response);
  }
  });
 }
	
 else
 {
  $( '#display_info' ).html("Please Enter Some Words");
 }
}

</script>
</head>



<body  onload="zoom()">

  <section id="container">

    <!-- **********************************************************************************************************************************************************

        TOP BAR CONTENT & NOTIFICATIONS

        *********************************************************************************************************************************************************** -->

    <!--header start-->

     <!--header start-->
    <?php
	include("header_t.php");
	?>
    <!--header end-->

    <!--header end-->

    <!-- **********************************************************************************************************************************************************

        MAIN SIDEBAR MENU

        *********************************************************************************************************************************************************** -->

		<?php //$userName = $_SESSION["name"];

        $userName = "SCTL"?>

    <!--sidebar start-->

    <aside>

      <div id="sidebar" class="nav-collapse ">

        <!-- sidebar menu start-->

        <ul class="sidebar-menu" id="nav-accordion">

          <p class="centered"><a href="#"><img src="img/sctl_logo.png" class="img-circle" width="80"></a></p>

          <h5 class="centered"><?php echo "User"; ?></h5>

          <li class="mt">

            <a href="index.php">

              <i class="fa fa-dashboard"></i>

              <span>Dashboard</span>

              </a>

          </li>

		  <li class="mt">

            <a href="projectRsearchAll.php" class="active">

              <i class="fa fa-dashboard"></i>

              <span>All Projects</span>

              </a>

          </li>

		  

		  

         

        </ul>

        <!-- sidebar menu end-->

      </div>

    </aside>

    <!--sidebar end-->

    <!-- **********************************************************************************************************************************************************

        MAIN CONTENT

        *********************************************************************************************************************************************************** -->

    <!--main content start-->

    <section id="main-content">

      <section class="wrapper">

       <!-- <h3><i class="fa fa-angle-right"></i> List of Complete SCP Projects (including Convergence)</h3>-->
        
<div class="row content-panel mt mb">
          <div class="col-md-6">
        <!-- <input type="text" name="username" id="username" onkeyup="loaddata();"> -->
        <label>DPR Status</label>
          <select name="username" id="username"  onchange="loaddata();" style="width:100%; height:35px">
<?php 
$sqlimg = "SELECT DISTINCT dpr_status FROM allprojects ";
                    
					//echo $sqlimg;
					$resultimg = mysqli_query($conn, $sqlimg);

					if (mysqli_num_rows($resultimg) > 0) {
						// output data of each row
						while($rowimg = mysqli_fetch_assoc($resultimg)) {
						echo "<option value=".$rowimg['dpr_status'].">".$rowimg['dpr_status']."</option>";
						}
						}

?>

            
            </select>
          

          
          </div>
          </div>
             

          <!-- page start-->

          <div class="content-panel" style="padding-top: unset;">
          
          
          <table cellpadding="0" cellspacing="0" border="0" class=" table table-bordered" style="background-color: #8090de;"  id="myTable">

                <thead>
        
                <tr>

                    <th style="text-align: center;width: 10px;">Sl. No</th>

                    <th style="text-align: center;width: 10px;">ID</th>

                    <th style="text-align: center;width: 150px;">Project Name</th>

                    <th class="hidden-phone" style="text-align: center;">SAR</th>

                    <th class="hidden-phone" style="text-align: center;">FR</th>

                    <th  onclick="sortTable()" class="hidden-phone" style="text-align: center; cursor:  pointer;">DPR Amt<br><div style="font-size: smaller;">(in Cr)</div></th>

                    <th  onclick="sortTable2()" class="hidden-phone" style="text-align: center;cursor:  pointer;">AS Amt<br><div style="font-size: smaller;">(in Cr)</div></th>

					<th  onclick="sortTable3()" class="hidden-phone" style="text-align: center;cursor:  pointer;">TS Amt<br><div style="font-size: smaller;">(in Cr)</div></th>

                    <th  onclick="sortTable4()" class="hidden-phone" style="text-align: center;cursor:  pointer;">Tender Amt<br><div style="font-size: smaller;">(in Cr)</div></th>

					<th  onclick="sortTable5()" class="hidden-phone" style="text-align: center;cursor:  pointer;">Work Awarded Amt<br><div style="font-size: smaller;">(in Cr)</div></th>

					<th  onclick="sortTable6()" class="hidden-phone" style="text-align: center;cursor:  pointer;">Work Completed Amt<br><div style="font-size: smaller;">(in Cr)</div></th>

                    <th  onclick="sortTable7()" class="hidden-phone" style="text-align: center;cursor:  pointer;">Physical Work Progress<br><div style="font-size: smaller;">(in %)</div></th>

                    <th  onclick="sortTable8()" class="hidden-phone" style="text-align: center;">Expenditure Progress<br><div style="font-size: smaller;">(in %)</div></th>

                    <th class="hidden-phone" style="text-align: center;">Present Status</th>

                    </tr>

                  

    </thead>

    <tbody id="display_info" >
    
   <!--<div id="display_info" ></div> -->
    
    </tbody>
    </table>
    </div>
</div>




<!--  bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb-->

      
        <!-- /row -->

      </section>

      <!-- /wrapper -->

    </section>

    <!-- /MAIN CONTENT -->

    <!--main content end-->

    <!--footer start-->

    <?php include("footer.php")?>

    <!--footer end-->

  </section>

  <!-- js placed at the end of the document so the pages load faster -->

  <script src="lib/jquery/jquery.min.js"></script>

  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>

  <script src="lib/bootstrap/js/bootstrap.min.js"></script>

  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>

  <script src="lib/jquery.scrollTo.min.js"></script>

  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>

  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>

  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>

  <!--common script for all pages-->

  <script src="lib/common-scripts.js"></script>

  <!--script for this page-->

  

  

  

  

  <script type="text/javascript">

  $(".aser").show()

  $(".showhr").click(function() {

    event.preventDefault();

    $(this).closest('tr').nextUntil("tr:has(.showhr)").toggle("slow", function() {});

});

  

    /* Formating function for row details */

    function fnFormatDetails(oTable, nTr) {

        

      if (str == "") {

        document.getElementById("txtHint").innerHTML = "";

        return;

    } else { 

        if (window.XMLHttpRequest) {

            // code for IE7+, Firefox, Chrome, Opera, Safari

            xmlhttp = new XMLHttpRequest();

        } else {

            // code for IE6, IE5

            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

        }

        xmlhttp.onreadystatechange = function() {

            if (this.readyState == 4 && this.status == 200) {

                document.getElementById("txtHint").innerHTML = this.responseText;

            }

        };

        //xmlhttp.open("GET","getuser.php?q="+str,true);

        xmlhttp.open("GET","getuser.php",true);

        xmlhttp.send();

    }

      

    }



    $(document).ready(function() {

      /*

       * Insert a 'details' column to the table

       */

      var nCloneTh = document.createElement('th');

      var nCloneTd = document.createElement('td');

      nCloneTd.innerHTML = '<img src="lib/advanced-datatable/images/details_open.png">';

      nCloneTd.className = "center";



      $('#hidden-table-info thead tr').each(function() {

        this.insertBefore(nCloneTh, this.childNodes[0]);

      });



      $('#hidden-table-info tbody tr').each(function() {

        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);

      });



      /*

       * Initialse DataTables, with no sorting on the 'details' column

       */

      var oTable = $('#hidden-table-info').dataTable({

        "aoColumnDefs": [{

          "bSortable": false,

          "aTargets": [0]

        }],

        "aaSorting": [

          [1, 'asc']

        ]

      });



      /* Add event listener for opening and closing details

       * Note that the indicator for showing which row is open is not controlled by DataTables,

       * rather it is done here

       */

      $('#hidden-table-info tbody td img').live('click', function() {

        var nTr = $(this).parents('tr')[0];

        if (oTable.fnIsOpen(nTr)) {

          /* This row is already open - close it */

          this.src = "lib/advanced-datatable/media/images/details_open.png";

          oTable.fnClose(nTr);

        } else {

          /* Open this row */

          this.src = "lib/advanced-datatable/images/details_close.png";

          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');

        }

      });

    });

  </script>

  <script type="text/javascript">

        function zoom() {

            document.body.style.zoom = "90%" 

        }

</script>

<script>

function sortTable() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[4];

      y = rows[i].getElementsByTagName("TD")[4];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable()

function sortTable2() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[5];

      y = rows[i].getElementsByTagName("TD")[5];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable2()

function sortTable3() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[6];

      y = rows[i].getElementsByTagName("TD")[6];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable3();

function sortTable4() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[7];

      y = rows[i].getElementsByTagName("TD")[7];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable4();

function sortTable5() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[8];

      y = rows[i].getElementsByTagName("TD")[8];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable5();

function sortTable6() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[9];

      y = rows[i].getElementsByTagName("TD")[9];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable6();

function sortTable7() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[10];

      y = rows[i].getElementsByTagName("TD")[10];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable7();

function sortTable8() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[11];

      y = rows[i].getElementsByTagName("TD")[11];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable8();

</script>



</body>



</html>

