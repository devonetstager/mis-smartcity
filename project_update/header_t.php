<header class="header black-bg"  style="background-color: #0171c7;">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>SCTL - <span>Project Management Dashboard</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" style="margin-left: unset;" id="top_menu">
        <!--  notification start -->
        <ul class="nav top-menu">
        <!-- settings start -->
          <li class="dropdown">
            
             <img src="https://www.smartcitytvm.in/wp-content/uploads/2020/01/sctl_s_logo.png" alt="">
          </li>
          <!-- settings end -->
          <!-- settings start -->
          <li class="dropdown">
            
            <img src="https://www.smartcitytvm.in/wp-content/themes/Netstager_Creative_Suite-3.0-beta/images/government-of-india.png" alt="">
          </li>
          <!-- settings end -->
          <!-- inbox dropdown start-->
          <li id="header_inbox_bar" class="dropdown">
             <img src="https://www.smartcitytvm.in/wp-content/themes/Netstager_Creative_Suite-3.0-beta/images/government-of-kerala.png" alt="">
          </li>
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->
          <li id="header_notification_bar" class="dropdown">
            <img src="https://www.smartcitytvm.in/wp-content/themes/Netstager_Creative_Suite-3.0-beta/images/corporation-of-thiruvananthapuram.png" alt="">
          </li>
          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="logout.php">Logout</a></li>
        </ul>
      </div>
    </header>