<?php
include("dbconfig/dbconfig.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>SCTL - Search 
  
  Projects</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
  
  
  ======================================================= -->
</head>

<?php

if(isset($_POST['submit']))

{

    $p_nameid = $_POST["p_nameid"];
	$sub_pnameid = $_POST["sub_pnameid"];
	$ward_num = $_POST["ward_num"];
	$fund_pattern = $_POST["fund_pattern"];
	$as_comm = $_POST["as_comm"];
	$ts_comm = $_POST["ts_comm"];
	$ctr_name = $_POST["ctr_name"]; 
	
	$sqlQ="SELECT project_details_with_fund.p_id, project_details_with_fund.p_name, contractor_details.ctr_name, contractor_details.ctr_add, as_approved.as_amt,as_approved.as_date,  as_approved.as_status,  as_approved.as_status,  as_approved.as_status
			FROM project_details_with_fund,  contractor_details, as_approved
			WHERE
   				 project_details_with_fund.p_id = '".$p_nameid."'";
				 

	

}

?>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>SCTL</b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <!--  notification end -->
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
   <?php $userName= $_SESSION["name"]; ?>
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="#"><img src="img/sctl_logo.png" class="img-circle" width="80"></a></p>
          <h5 class="centered"><?php echo $userName; ?></h5>
          <li class="mt">
            <a href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
		  
		  <li class="mt">
            <a href="searchAll.php">
              <i class="fa fa-dashboard"></i>
              <span>All Projects</span>
              </a>
          </li>
		  
		  <li class="mt">
            <a href="searchOngoing.php" class="active">
              <i class="fa fa-dashboard"></i>
              <span>Search Projects</span>
              </a>
          </li>
		  <li class="mt">
            <a href="scp_dpr_completed.php">
              <i class="fa fa-dashboard"></i>
              <span>DPR Completed Projects</span>
              </a>
          </li>
          <li class="mt">
            <a href="imageUpload.php">
              <i class="fa fa-dashboard"></i>
              <span>Image Upload</span>
              </a>
          </li>
         
		 
		 
		 
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3>SEARCH ONGOING PROJECTS</h3>
        <div class="row mt">
          <div class="col-lg-12">
          <div class="custom-box">
          
            <div class="form-panel">
              <form role="form" class="form-horizontal style-form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			  
			  
			  <div class="form-group">
                  <label class="col-lg-2 control-label">Project Name</label>
                  <div class="col-lg-10">
                   <?php
					 $sql = "SELECT * FROM allprojects";
					//echo $sql;
					$result = mysqli_query($conn, $sql);
					//echo $conn;
							if(mysqli_num_rows($result) > 0){
							$select= '<select style="width:70%" name="p_nameid"><option value="ALL">ALL</option>';
							while($row = mysqli_fetch_assoc($result)){
								  $select.='<option value="'.$row['sub_pid'].'":"'.$row['p_id'].'">'.$row['p_name'].' - '.$row['sub_pname'].'</option>';
							  }
							}
								$select.='</select>';
								echo $select;
					?>
                  </div>
                </div>
			  
			  
                
                
                <div class="form-group">
                  <label class="col-lg-2 control-label">Ward</label>
                  <div class="col-lg-3">
                    <?php
					 $sql3 = "SELECT * FROM ward_project";
					//echo $sql;
					$result3 = mysqli_query($conn, $sql3);
					//echo $conn;
							if(mysqli_num_rows($result3) > 0){
							$select3= '<select  style="width:50%" name="ward_num"><option value="ALL">ALL</option>';
							while($row3 = mysqli_fetch_assoc($result3)){
								  $select3.='<option value="'.$row3['ward_no'].'">'.$row3['ward_no'].' - '.$row3['ward_name'].'</option>';
							  }
							}
					$select3.='</select>';
					echo $select3;
					?>
                    
                 
                </div>
                
                 
                  <label class="col-lg-2 control-label">Funding Pattern</label>
                  <div class="col-lg-3">
                    <select style="width:50%" name="fund_pattern">
					<option value="ALL">ALL</option>
 					 <option value="grant">Smart City Grant</option>
 					 <option value="cs">Central Scheme</option>
				     <option value="ppp">PPP</option>
 					 <option value="ulb">ULB</option>
					</select>
                    
                  </div>
                  
                  
                </div>
                <div class="form-group">
                <label class="col-lg-2 control-label">Administrative Sanction Approvals</label>
                  <div class="col-lg-3">
                    <select style="width:50%" name="as_comm">
					<option value="ALL">ALL</option>
 					 <option value="ceo">CEO</option>
 					 <option value="subcommittee">Sub- Committee</option>
				     <option value="bod">Board of Directors</option>
					</select>
                    </div>
                   <label class="col-lg-2 control-label">Technical Sanction Approval</label>
                  <div class="col-lg-3">
                    <select style="width:50%" name="ts_comm">
					<option value="ALL">ALL</option>
 					 <option value="corp">Corporation</option>
 					 <option value="tscomm">TS Committee</option>
					</select>
                    </div>
                  </div>
                  
                  
                <div class="form-group">
                <label class="col-lg-2 control-label">Contractor</label>
                  <div class="col-lg-3">
                    <?php
					 $sql4 = "SELECT * FROM contractor_details";
					//echo $sql;
					$result4 = mysqli_query($conn, $sql4);
					//echo $conn;
							if(mysqli_num_rows($result4) > 0){
							$select4= '<select style="width:50%" name="ctr_name"><option value="ALL">ALL</option>';
							while($row4 = mysqli_fetch_assoc($result4)){
								  $select4.='<option value="'.$row4['ctr_id'].'">'.$row4['ctr_id'].' - '.$row4['ctr_name'].'</option>';
							  }
							}
					$select4.='</select>';
					echo $select4;
					?>
                    </div>
                  
                  
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-theme" type="submit" name="submit">Search</button>
                  </div>
                </div>
               
              </form>
            </div>
            <!-- /form-panel -->
         
            
            	
           
          </div>
            
            
			
                 
			
			
          </div>
		  <div class="col-lg-12">
		  <div class="row mb">
          <!-- page start-->
          <div class="content-panel">
            <div class="adv-table">
              <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Project Name</th>
                    <th class="hidden-phone">Execution Agency</th>
                    <th class="hidden-phone">Contractor Name</th>
                    <th class="hidden-phone">AS Amount</th>
					<th class="hidden-phone">Sanctioned Date</th>
                    <th class="hidden-phone">Status</th>
					<th class="hidden-phone">% Expenditure</th>
					<th class="hidden-phone">Remarks</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
				    //$sql = "SELECT * FROM project_details_with_fund";
					//echo $sql;
					$result = mysqli_query($conn, $sqlQ);

					if (mysqli_num_rows($result) > 0) {
						// output data of each row
						while($row = mysqli_fetch_assoc($result)) {
							
							echo "<tr>
									<td>".$row["p_id"]."</td>
									<td>".$row["p_name"]."</td>
									<td class='hidden-phone'>".$row["ctr_name"]."</td>
									<td class='center hidden-phone'>".$row["ctr_add"]."</td>
									<td class='center hidden-phone'>".round($row["p_cs"],2)."</td>
									<td class='center hidden-phone'>".round($row["p_ppp"],2)."</td>
									<td class='center hidden-phone'>".round($row["p_ulb"],2)."</td>
									<td class='center hidden-phone'>".$p_name."</td>
								  </tr>";						
		
						}
						
					} else {
						echo "0 results";
					}
					$conn->close();
				  
				  
				  
				  ?>
                 
                  
                  
                </tbody>
              </table>
            </div>
          </div>
          <!-- page end-->
        </div>
		  
		  </div>
		  
        </div>
		
		
		<!--table start->
		
		
		
		<!--table end-->
		
		
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php");?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->

</body>

</html>
