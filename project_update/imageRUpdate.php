<form role="form" class="form-horizontal style-form"   action="imageUpload_query.php" method="post" enctype="multipart/form-data">
   <table class="table table-bordered table-condensed">
      <tbody style="text-align: left;">
         <tr style="text-align: center;">
            <td style="text-align: center;"><input type="text" placeholder="" id="p_id"  name="p_id" class="form-control" readonly  value="<?php  echo $projectID; ?>"></td>
            <?php
               if($subprojectID == "")
               { $subprojectID = "NIL";
               }  
               
               ?>
            <td style="text-align: center;"><input type="text" placeholder="" id="sub_pid"  name="sub_pid" class="form-control" readonly  value="<?php echo $subprojectID; ?>"></td>
         </tr>
      </tbody>
   </table>
                
   <div class="form-group last">
      <label class="control-label col-md-3">Image Upload</label>
      <div class="col-md-9">
         <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
               <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" alt="" />
            </div>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
            <div>
               <span class="btn btn-theme02 btn-file">
               <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select image</span>
               <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
               <input type="file" class="default" name="projectImagestoUpload" id="projectImagestoUpload" />
               </span>
               <a href="advanced_form_components.html#" class="btn btn-theme04 fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a>
            </div>
         </div>
         <span class="label label-info">NOTE!</span>
         <span>
         Attached image thumbnail is
         supported in Latest Firefox, Chrome, Opera,
         Safari and Internet Explorer 10 only
         </span>
      </div>
   </div>
   <div class="form-group">
      <div class="col-lg-offset-2 col-lg-10">
         <button class="btn btn-theme" type="submit">Submit</button>
      </div>
   </div>
</form>