<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Admin - Project</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.html" class="logo"><b>DASH<span>IO</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav top-menu">
          <!-- settings start -->
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-tasks"></i>
              <span class="badge bg-theme">4</span>
              </a>
            <ul class="dropdown-menu extended tasks-bar">
              <div class="notify-arrow notify-arrow-green"></div>
              <li>
                <p class="green">You have 4 pending tasks</p>
              </li>
              <li>
                <a href="index.html#">
                  <div class="task-info">
                    <div class="desc">Dashio Admin Panel</div>
                    <div class="percent">40%</div>
                  </div>
                  <div class="progress progress-striped">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                      <span class="sr-only">40% Complete (success)</span>
                    </div>
                  </div>
                </a>
              </li>
              <li>
                <a href="index.html#">
                  <div class="task-info">
                    <div class="desc">Database Update</div>
                    <div class="percent">60%</div>
                  </div>
                  <div class="progress progress-striped">
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                      <span class="sr-only">60% Complete (warning)</span>
                    </div>
                  </div>
                </a>
              </li>
              <li>
                <a href="index.html#">
                  <div class="task-info">
                    <div class="desc">Product Development</div>
                    <div class="percent">80%</div>
                  </div>
                  <div class="progress progress-striped">
                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                      <span class="sr-only">80% Complete</span>
                    </div>
                  </div>
                </a>
              </li>
              <li>
                <a href="index.html#">
                  <div class="task-info">
                    <div class="desc">Payments Sent</div>
                    <div class="percent">70%</div>
                  </div>
                  <div class="progress progress-striped">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                      <span class="sr-only">70% Complete (Important)</span>
                    </div>
                  </div>
                </a>
              </li>
              <li class="external">
                <a href="#">See All Tasks</a>
              </li>
            </ul>
          </li>
          <!-- settings end -->
          <!-- inbox dropdown start-->
          <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-envelope-o"></i>
              <span class="badge bg-theme">5</span>
              </a>
            <ul class="dropdown-menu extended inbox">
              <div class="notify-arrow notify-arrow-green"></div>
              <li>
                <p class="green">You have 5 new messages</p>
              </li>
              <li>
                <a href="index.html#">
                  <span class="photo"><img alt="avatar" src="img/ui-zac.jpg"></span>
                  <span class="subject">
                  <span class="from">Zac Snider</span>
                  <span class="time">Just now</span>
                  </span>
                  <span class="message">
                  Hi mate, how is everything?
                  </span>
                  </a>
              </li>
              <li>
                <a href="index.html#">
                  <span class="photo"><img alt="avatar" src="img/ui-divya.jpg"></span>
                  <span class="subject">
                  <span class="from">Divya Manian</span>
                  <span class="time">40 mins.</span>
                  </span>
                  <span class="message">
                  Hi, I need your help with this.
                  </span>
                  </a>
              </li>
              <li>
                <a href="index.html#">
                  <span class="photo"><img alt="avatar" src="img/ui-danro.jpg"></span>
                  <span class="subject">
                  <span class="from">Dan Rogers</span>
                  <span class="time">2 hrs.</span>
                  </span>
                  <span class="message">
                  Love your new Dashboard.
                  </span>
                  </a>
              </li>
              <li>
                <a href="index.html#">
                  <span class="photo"><img alt="avatar" src="img/ui-sherman.jpg"></span>
                  <span class="subject">
                  <span class="from">Dj Sherman</span>
                  <span class="time">4 hrs.</span>
                  </span>
                  <span class="message">
                  Please, answer asap.
                  </span>
                  </a>
              </li>
              <li>
                <a href="index.html#">See all messages</a>
              </li>
            </ul>
          </li>
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->
          <li id="header_notification_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-bell-o"></i>
              <span class="badge bg-warning">7</span>
              </a>
            <ul class="dropdown-menu extended notification">
              <div class="notify-arrow notify-arrow-yellow"></div>
              <li>
                <p class="yellow">You have 7 new notifications</p>
              </li>
              <li>
                <a href="index.html#">
                  <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                  Server Overloaded.
                  <span class="small italic">4 mins.</span>
                  </a>
              </li>
              <li>
                <a href="index.html#">
                  <span class="label label-warning"><i class="fa fa-bell"></i></span>
                  Memory #2 Not Responding.
                  <span class="small italic">30 mins.</span>
                  </a>
              </li>
              <li>
                <a href="index.html#">
                  <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                  Disk Space Reached 85%.
                  <span class="small italic">2 hrs.</span>
                  </a>
              </li>
              <li>
                <a href="index.html#">
                  <span class="label label-success"><i class="fa fa-plus"></i></span>
                  New User Registered.
                  <span class="small italic">3 hrs.</span>
                  </a>
              </li>
              <li>
                <a href="index.html#">See all notifications</a>
              </li>
            </ul>
          </li>
          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="login.html">Logout</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Sam Soffes</h5>
          <li class="mt">
            <a href="index.html">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-desktop"></i>
              <span>UI Elements</span>
              </a>
            <ul class="sub">
              <li><a href="general.html">General</a></li>
              <li><a href="buttons.html">Buttons</a></li>
              <li><a href="panels.html">Panels</a></li>
              <li><a href="font_awesome.html">Font Awesome</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-cogs"></i>
              <span>Components</span>
              </a>
            <ul class="sub">
              <li><a href="grids.html">Grids</a></li>
              <li><a href="calendar.html">Calendar</a></li>
              <li><a href="gallery.html">Gallery</a></li>
              <li><a href="todo_list.html">Todo List</a></li>
              <li><a href="dropzone.html">Dropzone File Upload</a></li>
              <li><a href="inline_editor.html">Inline Editor</a></li>
              <li><a href="file_upload.html">Multiple File Upload</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a class="active" href="javascript:;">
              <i class="fa fa-book"></i>
              <span>Extra Pages</span>
              </a>
            <ul class="sub">
              <li class="active"><a href="blank.html">Blank Page</a></li>
              <li><a href="login.html">Login</a></li>
              <li><a href="lock_screen.html">Lock Screen</a></li>
              <li><a href="profile.html">Profile</a></li>
              <li><a href="invoice.html">Invoice</a></li>
              <li><a href="pricing_table.html">Pricing Table</a></li>
              <li><a href="faq.html">FAQ</a></li>
              <li><a href="404.html">404 Error</a></li>
              <li><a href="500.html">500 Error</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-tasks"></i>
              <span>Forms</span>
              </a>
            <ul class="sub">
              <li><a href="form_component.html">Form Components</a></li>
              <li><a href="advanced_form_components.html">Advanced Components</a></li>
              <li><a href="form_validation.html">Form Validation</a></li>
              <li><a href="contactform.html">Contact Form</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th"></i>
              <span>Data Tables</span>
              </a>
            <ul class="sub">
              <li><a href="basic_table.html">Basic Table</a></li>
              <li><a href="responsive_table.html">Responsive Table</a></li>
              <li><a href="advanced_table.html">Advanced Table</a></li>
            </ul>
          </li>
          <li>
            <a href="inbox.html">
              <i class="fa fa-envelope"></i>
              <span>Mail </span>
              <span class="label label-theme pull-right mail-info">2</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class=" fa fa-bar-chart-o"></i>
              <span>Charts</span>
              </a>
            <ul class="sub">
              <li><a href="morris.html">Morris</a></li>
              <li><a href="chartjs.html">Chartjs</a></li>
              <li><a href="flot_chart.html">Flot Charts</a></li>
              <li><a href="xchart.html">xChart</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-comments-o"></i>
              <span>Chat Room</span>
              </a>
            <ul class="sub">
              <li><a href="lobby.html">Lobby</a></li>
              <li><a href="chat_room.html"> Chat Room</a></li>
            </ul>
          </li>
          <li>
            <a href="google_maps.html">
              <i class="fa fa-map-marker"></i>
              <span>Google Maps </span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3></h3>
        <div class="row mt">
          <div class="col-lg-12">
            
            
			<div class="col-lg-12">
              <div class="col-md-4 col-sm-4 mb" style="width:100%">
                  <div class="green-panel">
                    <div class="green-header">
                      <h3>Project Title ##</h3>
                    </div>
                    <h5>District : Thiruvananthapuram</h5>
                    <h5>Amount(In Rs.) : ####</h5>
                    <h5>Expenditure (In Rs.) : ###</h5>
                    <h5>Executing Agency : ####</h5>
                    <h5>Status : ####</h5>
                    <h5>Remarks : ####</h5>
                    <h5>Remark Dated on ###</h5>
                  </div>
                </div>
               </div>
               
               <div class="col-lg-12">
                   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="custom-box">
                      <!-- WHITE PANEL - TOP USER -->
                      <div class="white-panel">
                        <div class="white-header">
                          <h3>Project Location</h3>
                        </div>
                        <div class="row">
                        <p></p>
                        </div>
                        <div class="row">
                        <p></p>
                        </div>
                        <div class="row black">
                          <div class="col-md-4">
                            <p class="small mt" style="color:#000">DISTRICT</p>
                            <p style="font-size:15px color:black">2012</p>
                          </div>
                          <div class="col-md-4">
                            <p class="small mt" style="color:#000" >ASSEMBLY CONSTITUENCY</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-4">
                            <p class="small mt" style="color:#000">LOCATION</p>
                            <p   >$ 47,60</p>
                          </div>
                          <div class="row">
                        	<p></p>
                          </div>
                          <div class="row">
                        	<p></p>
                          </div>
                          <div class="row">
                        	<p></p>
                          </div>
                          <div class="row">
                        	<p></p>
                          </div>
                        </div>
                      </div>
                      </div>
                    
                        <!-- end custombox -->
                        
                      
                  </div>
                  <!-- end col-8 -->
                  
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="custom-box">
                      <img src="img/ui-zac.jpg" height = "204" width ="300"></p>
                    </div>
                    <!-- end custombox -->
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="custom-box"> 
                      <!-- WHITE PANEL - TOP USER -->
                      <div class="white-panel">
                        <div class="white-header">
                          <h3>Sanction Details</h3>
                        </div>
                        <div class="row black">
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000">FILE NUMBER</p>
                            <p  >2012</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000">SANCTION ORDER & DATE</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000">ADDITIONAL SANCTION IF ANY:</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000">HEAD OF ACCOUNT</p>
                            <p  >$ 47,60</p>
                          </div>
                        </div>
                        
                        <div class="row ">
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000">PROJECT CATEGORY NAME</p>
                            <p >2012</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000">EXECUTING AGENCY</p>
                            <p >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >WORK CONTRACTOR</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt"  style="color:#000">ARCHITECT</p>
                            <p  >$ 47,60</p>
                          </div>
                        </div>
                        
                        <div class="row ">
                          <div class="col-md-3">
                            <p class="small mt"  style="color:#000" >AS AMOUNT (IN Rs.)</p>
                            <p  >2012</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt"  style="color:#000">AMOUNT RELEASED (IN Rs.)</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt"  style="color:#000">EXPENDITURE (IN Rs.)</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >COMPLETION DATE</p>
                            <p  >$ 47,60</p>
                          </div>
                        </div>
                        
                        <div class="row ">
                          <div class="col-md-12">
                            <p class="small mt" style="color:#000"  >PROJECT COMPONENTS/DESCRIPTION</p>
                            <p  >2012</p>
                          </div>
                          
                          </div>
                        </div>
                        
                      </div>
                    </div>
                        <!-- end custombox -->
                  </div>
                  <!-- end col-12 -->

                  
                  
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="custom-box"> 
                      <!-- WHITE PANEL - TOP USER -->
                      <div class="white-panel ">
                        <div class="white-header">
                          <h3>Project Details</h3>
                        </div>
                        <div class="row">
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >TYPE OF OPERATION</p>
                            <p  >2012</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >DATE OF OPERATION</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >MONTHLY AVERAGE INCOME (IN Rs.)</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >NUMBER OF VISITORS (PER MONTH)</p>
                            <p  >$ 47,60</p>
                          </div>
                        </div>
                        
                        <div class="row black">
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >TECHNICAL SANCTION ORDER NO AND DATE</p>
                            <p  >2012</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt"  style="color:#000">TENDER NUMBER AND DATE</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >AGREEMENT NO AND DATE</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            
                          </div>
                        </div>
                        
                        <div class="row ">
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >WORK STARTED</p>
                            <p  >2012</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >CIVIL WORK</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >LAND DEVELOPMENT</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >FOUNDATION WORKS</p>
                            <p  >$ 47,60</p>
                          </div>
                        </div>
                        
                        <div class="row ">
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000"  >STRUCTURE WORKS</p>
                            <p  >2012</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >ELECTRICITY AND PLUMBING</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000"  >BUILDING NUMBER</p>
                            <p  >$ 47,60</p>
                          </div>
                          <div class="col-md-3">
                            <p class="small mt" style="color:#000" >INAUGURATED</p>
                            <p  >$ 47,60</p>
                          </div>
                        </div>
                        
                        </div>
                      </div>
                    </div>
                        <!-- end custombox -->
                  </div>
                  <!-- end col-8 -->
                  
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                     <div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading">
                <ul class="nav nav-tabs nav-justified">
                  <li class="active">
                    <a data-toggle="tab" href="#overview">IMAGES</a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#contact" class="contact-map">DOCUMENTS</a>
                  </li>                 
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body">
                <div class="tab-content">
                  <div id="overview" class="tab-pane active">
                  
                  <div class="col-md-3">
                  
                   <div class="custom-box">
                      <img src="img/ui-zac.jpg" ></p>
                    </div>
                    <!-- end custombox -->
                    </div>
                  
                  
                  <div class="col-md-3">
                  
                   <div class="custom-box">
                      <img src="img/ui-zac.jpg"></p>
                    </div>
                    <!-- end custombox -->
                    </div>
                  
                  
                  <div class="col-md-3">
                  
                   <div class="custom-box">
                      <img src="img/ui-zac.jpg"></p>
                    </div>
                    <!-- end custombox -->
                    </div>
                  
                  
                  </div>
                  <!-- /tab-pane -->
                  <div id="contact" class="tab-pane">
                    <div class="col-md-3">
                  
                   <div class="custom-box">
                      <img src="img/ui-zac.jpg"></p>
                    </div>
                    <!-- end custombox -->
                    </div>
                  </div>
                  <!-- /tab-pane -->
                 
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>
                      
                        
                  </div>
                  <!-- end col-12 -->
                  
                  
                  
                  
               </div>
			
			
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">
          <!--
            You are NOT allowed to delete the credit link to TemplateMag with free version.
            You can delete the credit link only if you bought the pro version.
            Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/dashio-bootstrap-admin-template/
            Licensing information: https://templatemag.com/license/
          -->
          Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="blank.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->

</body>

</html>
