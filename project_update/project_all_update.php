<?php
include("dbconfig/dbconfig.php");
if(!isset($_SESSION)) 
    { 
        session_start(); 
        
    } 
    
if(!isset($_SESSION["name"])) {
	header("Location:login.php");
	}
$url_string1 = $_GET['link'];
$arr1 = explode(":",$_GET['link']);
$sub_pid =  $arr1[0];
$p_id = $arr1[1];

//if($arr[0] == "") echo "here 1";
//echo $arr[1];
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>SCTL - Project Update</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.html" class="logo"><b>SCTL</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
               <!--  notification end -->
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="login.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
<?php $userName = $_SESSION["name"]; ?>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="#"><img src="img/sctl_logo.png" class="img-circle" width="80"></a></p>
           <h5 class="centered"><?php echo $userName; ?></h5>          <li class="mt">
            <a href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="mt">
            <a href="searchAll.php">
              <i class="fa fa-dashboard"></i>
              <span>All Projects</span>
              </a>
          </li>
		  
		  <li class="mt">
            <a href="searchOngoing.php">
              <i class="fa fa-dashboard"></i>
              <span>Search Projects</span>
              </a>
          </li>
		  <li class="mt">
            <a href="scp_dpr_completed.php">
              <i class="fa fa-dashboard"></i>
              <span>DPR Completed Projects</span>
              </a>
          </li>
          <li class="mt">
            <a href="imageUpload.php">
              <i class="fa fa-dashboard"></i>
              <span>Image Upload</span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
        
        
    <!--main content start-->
    
    
    <section id="main-content">
      <section class="wrapper">
      
     				 <?php
							include("search_all_query.php");
			?>
      
        <h3><i class="fa fa-angle-right"></i> <?php echo $ProjectName; ?> </h3>
        <!-- BASIC FORM VALIDATION -->
        <div class="row mt">
          <div class="col-lg-12">
            <h4><i class="fa fa-angle-right"></i> <?php echo $subProjectName; ?> </h4>
            <h5><i class="fa fa-angle-right"></i> <?php echo "SCTL: ".$sctlOfficial; ?> </h5>
			<h5><i class="fa fa-angle-right"></i> <?php echo "PMC: ".$pmcOfficial; ?> </h5>
            <div class="form-panel">
              <form role="form" class="form-horizontal style-form"  method="post" action="project_all_update_query_new.php">
                
                <div class="form-group"  style="margin-left: 0px ;margin-right: 0px;">
                
                          <label class="col-lg-2 control-label">Project ID </label>
                          <div class="col-lg-2">
                            <input type="text" placeholder="" id="projectID"  name="projectID" class="form-control" readonly value="<?php  echo $projectID; ?>">
                            </div>
                            <label class="col-lg-2 control-label">Sub Project ID </label>
                    <div class="col-lg-2">
                      <input type="text" placeholder="" id="subprojectID"  name="subprojectID" class="form-control" readonly value="<?php  echo $subprojectID; ?>">
                    </div>
                     <label class="col-lg-2 control-label">Project Status </label>
                    <div class="col-lg-2">
                      <input type="text" placeholder="" id="projectStatus"  name="projectStatus" class="form-control"  value="<?php  echo $projectStatus; ?>">
                    </div>
                    </div>
                    <div class="form-group" style="border-bottom-color: #2f323a; margin-left: 0px ;margin-right: 0px;">
                
                          <label class="col-lg-2 control-label">Project Name </label>
                          <div class="col-lg-2">
                            <input type="text" placeholder="" id="ProjectName"  name="ProjectName" class="form-control" readonly value="<?php  echo $ProjectName; ?>">
                            </div>
                            <label class="col-lg-2 control-label">Sub Project Name </label>
                    <div class="col-lg-2">
                      <input type="text" placeholder="" id="subProjectName"  name="subProjectName" class="form-control" readonly value="<?php  echo $subProjectName; ?>">
                    </div>
                    </div>
                
                <?php
                include("dprUpdatePage.php");
                ?>
                  
                  <div class="form-group" style="border-bottom-color: #2f323a; margin-left: 0px ;margin-right: 0px;">
                  <h4 style="text-align: left; margin-left: 15px; text-decoration: underline;"><i class="fa"></i> Administrative Sanction Details</h4>
                  <label class="col-lg-1 control-label">AS Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="as_amount"  name="as_amount" class="form-control"  value="<?php  echo $as_amount; ?>">
                  </div>
                  
                  <label class="col-lg-1 control-label">AS Status </label>
                  <!--<div class="col-lg-2">
                    <input type="text" placeholder="" id="as_status"  name="as_status" class="form-control"  value="">
                    
                  </div>-->
                  <div class="col-lg-2">
                    <select name="as_status" style="width:100%; height:35px">
					<option value="<?php  echo $as_status; ?>"><?php  echo $as_status; ?></option>
                    <option value="">NIL</option>
 					 <option value="Submitted">Submitted</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Issued">Issued</option>
					</select>                    
                  </div>
                  <label class="col-lg-1 control-label">AS Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="as_orderno"  name="as_orderno" class="form-control"  value="<?php  echo $as_orderno; ?>">
                  </div>
                  <label class="col-lg-1 control-label">AS Issued Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="as_date"  name="as_date" class="form-control"  value="<?php  echo $as_date; ?>">
                  </div>
                  <!-- AS File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   
                   
                   <label class="col-lg-1 control-label">AS Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- AS File upload -->
                </div>
                
                <!-- TS details-->
                <?php
                include("tsDetailsUpdatePage.php");
                ?>
                 
                    <!-- Tender Details -->
                    <?php include("tenderDetailsUpdatePage.php");?>
                 <!-- Tender details end -->
                  
                  
                  
                
                <!-- Implemetation Details -->
                <?php
                include("implementationDetailsUpdatePage.php");
                ?>
                <!-- -->
                 
                
                
                
                <div class="form-group" style="margin-left: 0px ;margin-right: 0px;">
                  <h4 style="text-align: left; margin-left: 15px; text-decoration: underline;"><i class="fa"></i> Progress & Expenditure Details</h4>
                  <label class="col-lg-2 control-label">Expenditure Vs SCP Amount  </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exp_vs_scp"  name="exp_vs_scp" class="form-control" value="<?php  echo $exp_vs_scp; ?>">
                     
                  </div>
                  <label class="col-lg-2 control-label"> Expenditure Vs AS Amount</label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="exp_vs_as"  name="exp_vs_as" class="form-control" value="<?php  echo $exp_vs_as; ?>">
                     
                  </div>
                </div>
                <div class="form-group" style="border-bottom-color: #2f323a; margin-left: 0px ;margin-right: 0px;">
                  
                  <label class="col-lg-2 control-label">Physical %  </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="phy_perc"  name="phy_perc" class="form-control" value="<?php  echo $phy_perc; ?>">
                     
                  </div>
                  <label class="col-lg-2 control-label"> Financial %</label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="fin_perc"  name="fin_perc" class="form-control" value="<?php  echo $fin_perc; ?>">
                     
                  </div>
                </div>
                
                <div class="form-group" style="border-bottom-color: #2f323a; margin-left: 0px ;margin-right: 0px;">
                  
                  <label class="col-lg-2 control-label">SCTL Official  </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="sctlOfficial"  name="sctlOfficial" class="form-control" value="<?php  echo $sctlOfficial; ?>">
                     
                  </div>
                  <label class="col-lg-2 control-label"> PMC Official</label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="pmcOfficial"  name="pmcOfficial" class="form-control" value="<?php  echo $pmcOfficial; ?>">
                     
                  </div>
                </div>
                
                <div class="form-group" style="border-bottom-color: #2f323a; margin-left: 0px ;margin-right: 0px;">
                <label class="col-lg-2 control-label"> Ward Name No</label>
                	<div class="col-lg-2">
                    			<select name="wardnoname" style="width:100%; height:35px">
                                 <option value="<?php  echo $ward_noname; ?>"><?php  echo $ward_noname; ?></option>
                                 <option value="">NIL</option>
                                 <option value="27 - Palayam">27 - Palayam</option>
                                 <option value="81 - Thampanoor">81 - Thampanoor</option>
                                 <option value="80 - Fort">80 - Fort</option>
                                 <option value="83 - Sreekandeshwaram">83 - Sreekandeshwaram</option>
                                 <option value="28 - Thycaud">28 - Thycaud</option>
                                 <option value="82 - Vanjiyoor">82 - Vanjiyoor</option>
                                 <option value="29 - Vazhuthacaud">29 - Vazhuthacaud</option>
                                 <option value="43 - Valliyasala">43 - Valliyasala</option>
                                 <option value="71 - Chala">71 - Chala</option>
                                </select>
                    </div>
                </div>
                
                
                <div class="form-group" style="border-bottom-color: #2f323a; margin-left: 0px ;margin-right: 0px;">
                  
                  <label class="col-lg-2 control-label">Project Description  </label>
                  <div class="col-lg-8">
                   <!-- <input type="text" placeholder="" id="pDescription"  name="pDescription" class="form-control" value="">-->
                    <textarea style="width: 100%;margin-left: 50;" name="pDescription" id="pDescription" rows="20" cols="30"><?php  echo $pDescription; ?></textarea>
                     
                  </div>
                </div>
                      <!--   <div class="form-group" style="border-bottom-color: #2f323a; margin-left: 0px ;margin-right: 0px;">
                        		<label class="col-lg-1 control-label">Project Document  </label>
                                <div class="col-md-2">
                                <input type="file" name ="pDocuments" class="default">
                                </div>
                                <div class="col-lg-2">
                                <select name="pDocumentname" style="width:100%; height:35px">
                                 <option value="<?php  /**
 * echo $pDocumentname; 
 */?>"><?php  /**
 * echo $pDocumentname; 
 */?></option>
                                 <option value="">NIL</option>
                                 <option value="TS Order">TS Order</option>
                                 <option value="AS Order">AS Order</option>
                                 <option value="NIT">NIT</option>
                                 <option value="Work Order">Work Order</option>
                                 <option value="Work Completion">Work Completion</option>
                                </select>
                                </div>
                                
                        </div>
				-->

                <div class="form-group" style="border-bottom-color: #2f323a; margin-left: 0px ;margin-right: 0px;">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-theme" type="submit">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /form-panel -->
          </div>
          <!-- /col-lg-12 -->
        </div>
        <!-- /row -->
        <!-- FORM VALIDATION -->
        <div class="row mt">
          <!-- /col-lg-12 -->
</div>
        <!-- /row -->
        <div class="row mt">
          <!-- /col-lg-12 -->
</div>
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php");?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="lib/form-validation-script.js"></script>

</body>


