<?php

include("dbconfig/dbconfig.php")

?>

<!DOCTYPE html>

<html lang="en">



<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="">

  <meta name="author" content="Dashboard">

  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

  <title>SCTL - Search Projects</title>



  <!-- Favicons -->

  <link href="img/favicon.png" rel="icon">

  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">



  <!-- Bootstrap core CSS -->

  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!--external css-->

  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />

  <link href="lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />

  <link href="lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />

  <link rel="stylesheet" href="lib/advanced-datatable/css/DT_bootstrap.css" />

  <!-- Custom styles for this template -->

  <link href="css/style.css" rel="stylesheet">

  <link href="css/style-responsive.css" rel="stylesheet">



  <!-- =======================================================

    

  ======================================================= -->

</head>



<body  onload="zoom()">

  <section id="container">

    <!-- **********************************************************************************************************************************************************

        TOP BAR CONTENT & NOTIFICATIONS

        *********************************************************************************************************************************************************** -->

    <!--header start-->

     <!--header start-->
    <?php
	include("header_t.php");
	?>
    <!--header end-->

    <!--header end-->

    <!-- **********************************************************************************************************************************************************

        MAIN SIDEBAR MENU

        *********************************************************************************************************************************************************** -->

		<?php //$userName = $_SESSION["name"];

        $userName = "SCTL"?>

    <!--sidebar start-->

    <aside>

      <div id="sidebar" class="nav-collapse ">

        <!-- sidebar menu start-->

        <ul class="sidebar-menu" id="nav-accordion">

          <p class="centered"><a href="#"><img src="img/sctl_logo.png" class="img-circle" width="80"></a></p>

          <h5 class="centered"><?php echo "User"; ?></h5>

          <li class="mt">

            <a href="index.php">

              <i class="fa fa-dashboard"></i>

              <span>Dashboard</span>

              </a>

          </li>

		  <li class="mt">

            <a href="projectRsearchAll.php" class="active">

              <i class="fa fa-dashboard"></i>

              <span>All Projects</span>

              </a>

          </li>

		  

		  

         

        </ul>

        <!-- sidebar menu end-->

      </div>

    </aside>

    <!--sidebar end-->

    <!-- **********************************************************************************************************************************************************

        MAIN CONTENT

        *********************************************************************************************************************************************************** -->

    <!--main content start-->

    <section id="main-content">

      <section class="wrapper">

       <!-- <h3><i class="fa fa-angle-right"></i> List of Complete SCP Projects (including Convergence)</h3>-->

        <div class="row mb">

          <!-- page start-->

          <div class="content-panel" style="padding-top: unset;">

            <div class="adv-table">

             <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<!-- <table class="table table-striped reportTable">

 -->

 <?php

 include("dash_home_query.php");

 ?>

 <?php include("ind_summary.php");?>

                 

 <table cellpadding="0" cellspacing="0" border="0" class=" table table-bordered" style="background-color: #8090de;"  id="myTable">

    <thead>

        <tr>

                    <th style="text-align: center;width: 10px;">Sl. No</th>

                    <th style="text-align: center;width: 10px;">ID</th>

                    <th style="text-align: center;width: 150px;">Project Name</th>

                    <th class="hidden-phone" style="text-align: center;">SAR</th>

                    <th class="hidden-phone" style="text-align: center;">FR</th>

                    <th  onclick="sortTable()" class="hidden-phone" style="text-align: center; cursor:  pointer;">DPR Amt<br><div style="font-size: smaller;">(in Cr)</div></th>

                    <th  onclick="sortTable2()" class="hidden-phone" style="text-align: center;cursor:  pointer;">AS Amt<br><div style="font-size: smaller;">(in Cr)</div></th>

					<th  onclick="sortTable3()" class="hidden-phone" style="text-align: center;cursor:  pointer;">TS Amt<br><div style="font-size: smaller;">(in Cr)</div></th>

                    <th  onclick="sortTable4()" class="hidden-phone" style="text-align: center;cursor:  pointer;">Tender Amt<br><div style="font-size: smaller;">(in Cr)</div></th>

					<th  onclick="sortTable5()" class="hidden-phone" style="text-align: center;cursor:  pointer; background-color: #06be0c;">Work Awarded Amt<br><div style="font-size: smaller;">(in Cr)</div></th>

					<th  onclick="sortTable6()" class="hidden-phone" style="text-align: center;cursor:  pointer;">Work Completed Amt<br><div style="font-size: smaller;">(in Cr)</div></th>

                    <th  onclick="sortTable7()" class="hidden-phone" style="text-align: center;cursor:  pointer;">Physical Work Progress<br><div style="font-size: smaller;">(in %)</div></th>

                    <th  onclick="sortTable8()" class="hidden-phone" style="text-align: center;">Expenditure Progress<br><div style="font-size: smaller;">(in %)</div></th>

                    <th class="hidden-phone" style="text-align: center;">Present Status</th>

                    </tr>

                  

    </thead>

    <tbody>

    

    <?php

                    $slNum = 0;

				    $sql = "SELECT DISTINCT p_id FROM allprojects WHERE wa_amt NOT IN (0) ORDER BY dpr_amt REGEXP '^[A-Z]{2}' ASC, IF(p_id REGEXP '^[A-Z]{2}', LEFT(p_id, 2), LEFT(p_id, 1)),

           CAST(IF(p_id REGEXP '^[A-Z]{2}', RIGHT(p_id, LENGTH(p_id) - 2), RIGHT(p_id, LENGTH(p_id) - 1)) AS SIGNED)";

					$result = mysqli_query($conn, $sql);

					if (mysqli_num_rows($result) > 0) {

						// output data of each row

						while($row = mysqli_fetch_assoc($result)) {

						  $slNum = $slNum + 1;

						/**

                         *  DPR sum

                         */

                        $sqldprsum = "SELECT SUM(dpr_amt) AS dprSum FROM allprojects WHERE p_id='".$row["p_id"]."'";

						$resultsprsum = mysqli_query($conn, $sqldprsum);

						$rowdprsum = mysqli_fetch_assoc($resultsprsum);

                        if(round($rowdprsum["dprSum"],2)>0)

                        { $rowdprsum1 = round($rowdprsum["dprSum"],2);

                         $colordpr ="limegreen";

                         

                          }

                        else 

                        {$rowdprsum1 = "";

                        $colordpr ="#ccc7c7";

                        

                        }

                        

                        

                        

                        /**

						 * 

						 */

                         $sqlfrstat = "SELECT p_id FROM allprojects WHERE dpr_status='FR'";

						$resultsfrstat = mysqli_query($conn, $sqlfrstat);

						$rowfrstat = mysqli_fetch_assoc($resultsfrstat);

                         

                        /**

                         *  More details of projectss

                         */

                        $sqlprojDetail = "SELECT * FROM allprojects WHERE p_id='".$row["p_id"]."'";

						$resultprojDetail = mysqli_query($conn, $sqlprojDetail);

						$rowprojDetail = mysqli_fetch_assoc($resultprojDetail);

                        

						/**

						 * 

						 */

                         /**

                         *  AS sum

                         */

                        $sqlASsum = "SELECT SUM(as_amt) AS ASSum FROM allprojects WHERE p_id='".$row["p_id"]."'";

						$resultsASsum = mysqli_query($conn, $sqlASsum);

						$rowASsum = mysqli_fetch_assoc($resultsASsum);

                        if(round($rowASsum["ASSum"],2)>0){ 

                            $rowASsum1 = round($rowASsum["ASSum"],2);

                            $coloras ="limegreen";

                             }else {

                                $rowASsum1 = "";

                                $coloras ="#ccc7c7";

                        }

                        /**

						 * 

						 */

                         /**

                         *  TS sum

                         */

                        $sqlTSsum = "SELECT SUM(ts_amt) AS TSSum FROM allprojects WHERE p_id='".$row["p_id"]."'";

						$resultsTSsum = mysqli_query($conn, $sqlTSsum);

						$rowTSsum = mysqli_fetch_assoc($resultsTSsum);

                        if(round($rowTSsum["TSSum"],2)>0){ 

                            $rowTSsum1 = round($rowTSsum["TSSum"],2);

                            $colorts ="limegreen";

                             }else {

                                $rowTSsum1 = "";

                                $colorts ="#ccc7c7";

                                }

                        

                        /**

						 * 

						 */

                         /**

                         *  Tender sum

                         */

                        $sqlTendersum = "SELECT SUM(tender_amt) AS TenderSum FROM allprojects WHERE p_id='".$row["p_id"]."'";

						$resultsTendersum = mysqli_query($conn, $sqlTendersum);

						$rowTendersum = mysqli_fetch_assoc($resultsTendersum);

                        if(round($rowTendersum["TenderSum"],2)>0){ 

                            $rowTendersum1 = round($rowTendersum["TenderSum"],2); 

                            $colortrsum ="limegreen";

                            }else {

                              $rowTendersum1 = "";  

                              $colortrsum ="#ccc7c7";

                            }

                         

                        /**

						 * 

						 */

                         /**

                         *  Work awarded sum

                         */

                        $sqlWorkAwardsum = "SELECT SUM(wa_amt) AS workawardedSum FROM allprojects WHERE p_id='".$row["p_id"]."'";

						$resultsWorkAwardsum = mysqli_query($conn, $sqlWorkAwardsum);

						$rowWorkAwardsum = mysqli_fetch_assoc($resultsWorkAwardsum);

                        if(round($rowWorkAwardsum["workawardedSum"],2)>0){ 

                            $workawardedSum1 = round($rowWorkAwardsum["workawardedSum"],2); 

                            $colorwasum ="#00ff08";

                            }else 

                            {$workawardedSum1 = "";

                            $colorwasum ="#ccc7c7";

                            }

                         

                        /**

						 * 

						 */

                         /**

                         *  Work Completion sum

                         */

                        $sqlworkCompletionSum = "SELECT SUM(wc_amt) AS workCompletionSum FROM allprojects WHERE p_id='".$row["p_id"]."'";

						$resultsCompletionSum = mysqli_query($conn, $sqlworkCompletionSum);

						$rowCompletionSum = mysqli_fetch_assoc($resultsCompletionSum);

                        if(round($rowCompletionSum["workCompletionSum"],2)>0){ 

                            $workCompletionSum1 = round($rowCompletionSum["workCompletionSum"],2);

                            $colorwcsum ="limegreen"; 

                            }else {

                            $workCompletionSum1 = "";

                            $colorwcsum ="#ccc7c7";

                            }

    

                        /**

						 * 

						 */

                         /**

                         *  Project Status

                         */

                        $sqlProjectStatus = "SELECT p_status FROM allprojects WHERE p_id='".$row["p_id"]."'";

						$resultsProjectStatus = mysqli_query($conn, $sqlProjectStatus);

						$rowProjectStatus = mysqli_fetch_assoc($resultsProjectStatus);

                        

                        /**

						 * 

						

                        

                        /**

						 * Condition for update details

						 */

                         if($rowprojDetail["sub_pid"] == '') //Also tried this "if(strlen($strTemp) > 0)"

                            {

                                 $UpdateDet = "<a href='project_all_update.php?link=:".$rowprojDetail["p_id"]."'>Update</a>";

                            } else $UpdateDet =  "";

                        

                         /**

                          * 

                          */

                          /**

						 * Condition for More details

						 */

                         if($rowprojDetail["sub_pid"] == '') //Also tried this "if(strlen($strTemp) > 0)"

                            {

                                 $moreDet = "";

                                 $url = "project.php?link=".$rowprojDetail["sub_pid"].":".$rowprojDetail["p_id"]."";

                                 $urlsub = "#";

                            } else 

                            {

                                $url = "#";

                                $urlsub = "project.php?link=".$rowprojDetail["sub_pid"].":".$rowprojDetail["p_id"]."";

                            $moreDet =  "&nbsp;&nbsp;&nbsp; <div style='color:black;'>Click for Sub projects</div>";

                        }

                         /**

                          * 

                          */

                          if($rowprojDetail["pw_prog"]){

                                $colorpw="limegreen";

                            }else $colorpw="#ccc7c7";

                            

                            if($rowprojDetail["exp_prog"]){

                                $colorexp="limegreen";

                            }else $colorexp="#ccc7c7";

                            

                            if($rowprojDetail["SAR"] == 'Submitted'){

                                $colorsar = "limegreen";

                                $tickM = "&#10004;";

                            }else {

                                $colorsar = "#ccc7c7";

                                $tickM = "&#10060;";

                                }

                            if($rowprojDetail["FR"] == 'Submitted'){

                                $colorfr = "limegreen";

                                $tickfr = "&#10004;";

                            }else {

                                $colorfr = "#ccc7c7";

                                $tickfr = "&#10060;";

                                }

                       

                            

                         

        echo "<tr style='background-color:#ccc7c7'>

            <td style='width : 10px;font-weight: 900; text-align: center;'>".$slNum."</td>

            <td style='width : 10px;font-weight: 900;'><a class='showhr' href='#'>".$row["p_id"]."</td>

            <td style='width : 150px;font-weight: 900;'><b><a href='$url'>".$rowprojDetail["p_name"]."</a></b><br><a class='showhr' href='#'>".$moreDet."</a></td>

            <td style='width : 70px; background-color: ".$colorsar."; text-align: center'>".$tickM."</td>

            <td style='width : 70px; background-color: ".$colorfr."; text-align: center'>".$tickfr."</td>

            <td style='width : 70px; background-color: ".$colordpr."; text-align: center'>".$rowdprsum1."</td>

            <td style='width : 70px; background-color: ".$coloras."; text-align: center'>".$rowASsum1."</td>

            <td style='width : 70px; background-color: ".$colorts."; text-align: center'>".$rowTSsum1."</td>

            <td style='width : 70px; background-color: ".$colortrsum."; text-align: center'>".$rowTendersum1."</td>

            <td style='width : 70px; background-color: ".$colorwasum."; text-align: center'>".$workawardedSum1."</td>

            <td style='width : 70px; background-color: ".$colorwcsum."; text-align: center'>".$workCompletionSum1."</td>

            <td style='width : 70px; background-color: ; text-align: center'>".$rowprojDetail["pw_prog"]."</td>

            <td style='width : 70px; background-color: ; text-align: center'>".$rowprojDetail["exp_as_cost"]."</td>

            <td style='width : 70px; ; text-align: center; word-break:break-all;'>".$rowprojDetail["p_status"]."</td>

            

        </tr>

        ";

        $workCompletionSum1 = 0;
        
        $sqlsub = "SELECT * FROM allprojects WHERE p_id='".$row["p_id"]."' AND wa_amt NOT IN (0)  ORDER BY LENGTH(sub_pid), sub_pid  ";

      //  $sqlsub = "SELECT * FROM allprojects WHERE p_id='".$row["p_id"]."'";

		$resultsub = mysqli_query($conn, $sqlsub);



					if (mysqli_num_rows($resultsub) > 0) {					   

						// output data of each row

						while($rowsub = mysqli_fetch_assoc($resultsub)) {

                        if($rowsub["sub_pid"]){	

                            $slNum = $slNum + 1;

                            if($rowsub["ts_amt"]){

                                $colorts="#82f382";

                            }else $colorts="#ccc7c7";

                            

                            if($rowsub["dpr_amt"]){

                                $colordpr="#82f382";

                                

                                

                            }else{ $colordpr="#ccc7c7";

                            

                            }

                            

                            if($rowsub["SAR"] == 'Submitted'){

                                $colorsar="#82f382";

                                $tickM = "&#10004;";

                                

                            }else{ 

                                $colorsar="#ccc7c7";

                                $tickM = "&#10060;";

                            }

                            

                            if($rowsub["FR"] == 'Submitted'){

                                $colorfr="#82f382";

                                $tickMFR = "&#10004;";

                                

                            }else{ 

                                $colorfr="#ccc7c7";

                                $tickMFR = "&#10060;";

                            }

                            

                            if($rowsub["as_amt"]){

                                $coloras="#82f382";

                            }else $coloras="#ccc7c7";

                            

                            if($rowsub["wa_amt"]){

                                $colorwasum="#00ff08";

                            }else $colorwasum="#ccc7c7";

                            

                            if($rowsub["wc_amt"]){

                                $colorwcsum="#82f382";

                            }else $colorwcsum="#ccc7c7";

                            

                            if($rowsub["tender_amt"]){

                                $colortrsum="#82f382";

                            }else $colortrsum="#ccc7c7";

                            

                             if($rowsub["pw_prog"]){

                                $colorpw="#ccc7c7";

                            }else $colorpw="#ccc7c7";

                            

                            if($rowsub["exp_prog"]){

                                $colorexp="#82f382";

                            }else $colorexp="#ccc7c7";

                            

                            

                         echo "

                         

                                   <tr class='aser' style='background-color:rgb(241, 241, 241)'>  

                                   <!--child row-->

                                   <td style='width : 10px;text-align: center;font-weight: 900;'>".$slNum."</td>

                                    <td style='width : 10px;text-align: right;'>&#8594;	".$rowsub["sub_pid"]."</td>

                                    <td style='width : 150px;text-align: right;'><a href='$urlsub'>".$rowsub["sub_pname"]."</a></td>

                                    <td style='width : 70px; background-color: ".$colorsar.";text-align: center'>".$tickM."</td>

                                    <td style='width : 70px; background-color: ".$colorfr.";text-align: center'>".$tickMFR."</td>

                                    <td style='width : 70px; background-color: ".$colordpr."; text-align: center'>".$rowsub["dpr_amt"]."</td>

                                    <td style='width : 70px; background-color: ".$coloras."; text-align: center'>".$rowsub["as_amt"]."</td>

                                    <td style='width : 70px; background-color: ".$colorts."; text-align: center'>".$rowsub["ts_amt"]."</td>

                                    <td style='width : 70px; background-color: ".$colortrsum."; text-align: center'>".$rowsub["tender_amt"]."</td>

                                    <td style='width : 70px; background-color: ".$colorwasum."; text-align: center'>".$rowsub["wa_amt"]."</td>

                                    <td style='width : 70px; background-color: ".$colorwcsum."; text-align: center'>".$rowsub["wc_amt"]."</td>

                                    <td style='width : 70px; background-color: ".$colorpw."; text-align: center'>".$rowsub["pw_prog"]."</td>

                                    <td style='width : 70px; background-color: ".$colorexp."; text-align: center'>".$rowsub["exp_prog"]."</td>

                                    <td style='width : 70px; background-color: #ccc7c7; ; text-align: center; word-break:break-all;'>".$rowsub["p_status"]."</td>

                                      </tr>                                

                                  

                                ";}  

                                                                        }

                                                        }?>

        

        

        <?php 

        	}

						

					} else {

						echo "0 results";

					}

					

					

					$conn->close();

				  

				  

				  

				  ?>

                  

    </tbody>

</table>

            </div>

            

          </div>

          <!-- page end-->

        </div>

        <!-- /row -->

      </section>

      <!-- /wrapper -->

    </section>

    <!-- /MAIN CONTENT -->

    <!--main content end-->

    <!--footer start-->

    <?php include("footer.php")?>

    <!--footer end-->

  </section>

  <!-- js placed at the end of the document so the pages load faster -->

  <script src="lib/jquery/jquery.min.js"></script>

  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>

  <script src="lib/bootstrap/js/bootstrap.min.js"></script>

  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>

  <script src="lib/jquery.scrollTo.min.js"></script>

  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>

  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>

  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>

  <!--common script for all pages-->

  <script src="lib/common-scripts.js"></script>

  <!--script for this page-->

  

  

  

  

  <script type="text/javascript">

  $(".aser").show()

  $(".showhr").click(function() {

    event.preventDefault();

    $(this).closest('tr').nextUntil("tr:has(.showhr)").toggle("slow", function() {});

});

  

    /* Formating function for row details */

    function fnFormatDetails(oTable, nTr) {

        

      if (str == "") {

        document.getElementById("txtHint").innerHTML = "";

        return;

    } else { 

        if (window.XMLHttpRequest) {

            // code for IE7+, Firefox, Chrome, Opera, Safari

            xmlhttp = new XMLHttpRequest();

        } else {

            // code for IE6, IE5

            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

        }

        xmlhttp.onreadystatechange = function() {

            if (this.readyState == 4 && this.status == 200) {

                document.getElementById("txtHint").innerHTML = this.responseText;

            }

        };

        //xmlhttp.open("GET","getuser.php?q="+str,true);

        xmlhttp.open("GET","getuser.php",true);

        xmlhttp.send();

    }

      

    }



    $(document).ready(function() {

      /*

       * Insert a 'details' column to the table

       */

      var nCloneTh = document.createElement('th');

      var nCloneTd = document.createElement('td');

      nCloneTd.innerHTML = '<img src="lib/advanced-datatable/images/details_open.png">';

      nCloneTd.className = "center";



      $('#hidden-table-info thead tr').each(function() {

        this.insertBefore(nCloneTh, this.childNodes[0]);

      });



      $('#hidden-table-info tbody tr').each(function() {

        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);

      });



      /*

       * Initialse DataTables, with no sorting on the 'details' column

       */

      var oTable = $('#hidden-table-info').dataTable({

        "aoColumnDefs": [{

          "bSortable": false,

          "aTargets": [0]

        }],

        "aaSorting": [

          [1, 'asc']

        ]

      });



      /* Add event listener for opening and closing details

       * Note that the indicator for showing which row is open is not controlled by DataTables,

       * rather it is done here

       */

      $('#hidden-table-info tbody td img').live('click', function() {

        var nTr = $(this).parents('tr')[0];

        if (oTable.fnIsOpen(nTr)) {

          /* This row is already open - close it */

          this.src = "lib/advanced-datatable/media/images/details_open.png";

          oTable.fnClose(nTr);

        } else {

          /* Open this row */

          this.src = "lib/advanced-datatable/images/details_close.png";

          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');

        }

      });

    });

  </script>

  <script type="text/javascript">

        function zoom() {

            document.body.style.zoom = "90%" 

        }

</script>

<script>

function sortTable() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[4];

      y = rows[i].getElementsByTagName("TD")[4];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable()

function sortTable2() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[5];

      y = rows[i].getElementsByTagName("TD")[5];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable2()

function sortTable3() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[6];

      y = rows[i].getElementsByTagName("TD")[6];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable3();

function sortTable4() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[7];

      y = rows[i].getElementsByTagName("TD")[7];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable4();

function sortTable5() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[8];

      y = rows[i].getElementsByTagName("TD")[8];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable5();

function sortTable6() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[9];

      y = rows[i].getElementsByTagName("TD")[9];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable6();

function sortTable7() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[10];

      y = rows[i].getElementsByTagName("TD")[10];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable7();

function sortTable8() {

  var table, rows, switching, i, x, y, shouldSwitch;

  table = document.getElementById("myTable");

  switching = true;

  /*Make a loop that will continue until

  no switching has been done:*/

  while (switching) {

    //start by saying: no switching is done:

    switching = false;

    rows = table.rows;

    /*Loop through all table rows (except the

    first, which contains table headers):*/

    for (i = 1; i < (rows.length - 1); i++) {

      //start by saying there should be no switching:

      shouldSwitch = false;

      /*Get the two elements you want to compare,

      one from current row and one from the next:*/

      x = rows[i + 1].getElementsByTagName("TD")[11];

      y = rows[i].getElementsByTagName("TD")[11];

      //check if the two rows should switch place:

      if (Number(x.innerHTML) > Number(y.innerHTML)) {

        //if so, mark as a switch and break the loop:

        shouldSwitch = true;

        break;

      }

    }

    if (shouldSwitch) {

      /*If a switch has been marked, make the switch

      and mark that a switch has been done:*/

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);

      switching = true;

    }

  }

}

//sortTable8();

</script>



</body>



</html>

