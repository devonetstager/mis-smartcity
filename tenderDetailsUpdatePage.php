<div class="form-group" style="border-bottom-color: #2f323a; margin-left: 0px ;margin-right: 0px;">
                <?php include("tenderQuery.php");?>
                 <h4 style="text-align: left; margin-left: 15px; text-decoration: underline;"><i class="fa"></i> Tender Details</h4>
                <div class="row content-panel" style="margin-left: 0px;margin-right: 0px;">
              <div class="panel-heading">
                <ul class="nav nav-tabs nav-justified">
                  <li class="active">
                    <a data-toggle="tab" href="#tcivil">Civil</a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#telec" class="contact-map">Electrical</a>
                  </li>   
                  <li>
                    <a data-toggle="tab" href="#tit" class="contact-map">IT</a>
                  </li>    
                  <li>
                    <a data-toggle="tab" href="#toth" class="contact-map">Other</a>
                  </li>              
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body">
                <div class="tab-content">
                  <div id="tcivil" class="tab-pane active">
                            
                             
                  <label class="col-lg-1 control-label">tender Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_civil_amt"  name="tender_civil_amt" class="form-control" value="<?php  echo $tender_civil_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">tender Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_status"  name="tender_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="tender_civil_status" style="width:100%; height:35px">
					 <option value="<?php  echo $tender_civil_status; ?>"><?php  echo $tender_civil_status; ?></option>
                     <option value="">NIL</option>
 					 <option value="RFP Submitted">RFP Submitted</option>
                     <option value="RFP Revisison">RFP Revisison</option>
                     <option value="Tender in Progress">Tender in Progress</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Completed">Completed</option>
                     <option value="Re - Tendered">Re - Tendered</option>
                     <option value="Cancelled">Cancelled</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">tender Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_civil_orderno"  name="tender_civil_orderno" class="form-control" value="<?php  echo $tender_civil_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">tender Start Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_civil_sdate"  name="tender_civil_sdate" class="form-control" value="<?php  echo $tender_civil_sdate; ?>">
                  </div>
                  <!-- tender File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   <label class="col-lg-1 control-label">tender End Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_civil_edate"  name="tender_civil_edate" class="form-control" value="<?php  echo $tender_civil_edate; ?>">
                  </div>
                   
                   <label class="col-lg-1 control-label">tender Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-existender"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-existender" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- tender File upload -->      
                  </div><!-- tender details-->
                  
                  <!-- tender Elec details-->
                  <!-- /tab-pane -->
                  <div id="telec" class="tab-pane">
                   
                  <label class="col-lg-1 control-label">tender Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_ele_amt"  name="tender_ele_amt" class="form-control" value="<?php  echo $tender_ele_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">tender Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_status"  name="tender_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="tender_ele_status" style="width:100%; height:35px">
					 <option value="<?php  echo $tender_ele_status; ?>"><?php  echo $tender_ele_status; ?></option>
                    <option value="">NIL</option>
 					 <option value="RFP Submitted">RFP Submitted</option>
                     <option value="RFP Revisison">RFP Revisison</option>
                     <option value="Tender in Progress">Tender in Progress</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Completed">Completed</option>
                     <option value="Re - Tendered">Re - Tendered</option>
                     <option value="Cancelled">Cancelled</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">tender Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_ele_orderno"  name="tender_ele_orderno" class="form-control" value="<?php  echo $tender_ele_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">tender Start Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_ele_sdate"  name="tender_ele_sdate" class="form-control" value="<?php  echo $tender_ele_sdate; ?>">
                  </div>
                  
                  <!-- tender File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   <label class="col-lg-1 control-label">Tender End Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_ele_edate"  name="tender_ele_edate" class="form-control" value="<?php  echo $tender_ele_edate; ?>">
                  </div>
                   
                   <label class="col-lg-1 control-label">tender Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-existender"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-existender" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- tender File upload -->
                   
                  </div>
                  <!-- /tab-pane -->
                  <!-- /tab-pane -->
                  <div id="tit" class="tab-pane">
                   
                   <label class="col-lg-1 control-label">tender Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_it_amt"  name="tender_it_amt" class="form-control" value="<?php  echo $tender_it_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">tender Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_status"  name="tender_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="tender_it_status" style="width:100%; height:35px">
					 <option value="<?php  echo $tender_it_status; ?>"><?php  echo $tender_it_status; ?></option>
                     <option value="">NIL</option>
 					 <option value="RFP Submitted">RFP Submitted</option>
                     <option value="RFP Revisison">RFP Revisison</option>
                     <option value="Tender in Progress">Tender in Progress</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Completed">Completed</option>
                     <option value="Re - Tendered">Re - Tendered</option>
                     <option value="Cancelled">Cancelled</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">tender Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_it_orderno"  name="tender_it_orderno" class="form-control" value="<?php  echo $tender_it_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">tender Start Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_it_sdate"  name="tender_it_sdate" class="form-control" value="<?php  echo $tender_it_sdate; ?>">
                  </div>
                  <!-- tender File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   
                   <label class="col-lg-1 control-label">tender End Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_it_edate"  name="tender_it_edate" class="form-control" value="<?php  echo $tender_it_edate; ?>">
                  </div>
                   
                   <label class="col-lg-1 control-label">tender Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-existender"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-existender" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- tender File upload -->
                   
                  </div>
                  <!-- /tab-pane -->
                  <!-- /tab-pane -->
                  <div id="toth" class="tab-pane">
                   
                  <label class="col-lg-1 control-label">tender Amount </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_other_amt"  name="tender_other_amt" class="form-control" value="<?php  echo $tender_other_amt; ?>">
                  </div>
                  <label class="col-lg-1 control-label">tender Status </label>
                 <!-- <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_status"  name="tender_status" class="form-control"  value="">                   
                  </div>-->
                  <div class="col-lg-2">
                    <select name="tender_other_status" style="width:100%; height:35px">
					 <option value="<?php  echo $tender_other_status; ?>"><?php  echo $tender_other_status; ?></option>
                     <option value="">NIL</option>
 					 <option value="RFP Submitted">RFP Submitted</option>
                     <option value="RFP Revisison">RFP Revisison</option>
                     <option value="Tender in Progress">Tender in Progress</option>
 					 <option value="On Hold">On Hold </option>
				     <option value="Completed">Completed</option>
                     <option value="Re - Tendered">Re - Tendered</option>
                     <option value="Cancelled">Cancelled</option>
					</select>                    
                  </div>
                   <label class="col-lg-1 control-label">tender Order No </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_other_orderno"  name="tender_other_orderno" class="form-control" value="<?php  echo $tender_other_orderno; ?>">
                  </div>
                   <label class="col-lg-1 control-label">tender Start Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_other_sdate"  name="tender_other_sdate" class="form-control" value="<?php  echo $tender_other_sdate; ?>">
                  </div>
                  <!-- tender File upload -->
                  <div class="col-lg-2" style="width: -webkit-fill-available; margin-left: -15px; margin-top: 15px;">
                   <label class="col-lg-1 control-label">tender End Date </label>
                  <div class="col-lg-2">
                    <input type="text" placeholder="" id="tender_other_edate"  name="tender_other_edate" class="form-control" value="<?php  echo $tender_other_edate; ?>">
                  </div>
                   
                   <label class="col-lg-1 control-label">tender Order document </label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select file</span>
                      <span class="fileupload-existender"><i class="fa fa-undo"></i> Change</span>
                      <input type="file" class="default" />
                      </span>
                      <span class="fileupload-preview" style="margin-left:5px;"></span>
                      <a href="#" class="close fileupload-existender" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                    </div>
                  </div>
                    <!-- tender File upload -->
                   
                  </div>
                  <!-- /tab-pane -->
                 
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
                 </div>