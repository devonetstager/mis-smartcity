
        <!-- page start-->
        <div id="morris">
          <div class="row mt">
            <div class="col-lg-6">
            <h3>Project Progress as on 20.01.2020 ( in INR Cr)</h3>
             <div id="chartContainer" style="height: 370px; width: 100%;"></div>
            </div>
            <div class="col-lg-6">
            <h3>Grant Allocatted Vs Received (in INR Cr)</h3>
             <div id="chartContainer2" style="height: 370px; width: 100%;"></div>
            </div>
            </div>
            <div class="row mt">
            <div class="col-lg-6">
            <h3>Total expenditure incurred against Smart City fund allocation (In INR Crores)</h3>
             <div id="chartContainer3" style="height: 370px; width: 100%;"></div>
            </div>
            <div class="col-lg-6">
            <h3>Total expenditure incurred against Smart City fund allocation (In percentage)</h3>
             <div id="chartContainer4" style="height: 370px; width: 100%;"></div>
            </div>
            </div>
         
        </div>
        <!-- page end-->
      
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="lib/raphael/raphael.min.js"></script>
  <script src="lib/morris/morris.min.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="lib/morris-conf.js"></script>
<script>
window.onload = function () {
var chart1 = new CanvasJS.Chart("chartContainer", {
	theme:"light2",
	animationEnabled: true,
	title:{
		text: ""
	},
	axisY :{
		includeZero: false,
		title: "Amount in INR Cr",
		prefix: "Rs",
		maximum: 1538,
		interval: 200
	},
	toolTip: {
		shared: "true"
	},
	legend:{
		cursor:"pointer",
		itemclick : toggleDataSeries
	},
	data: [
	
		
		{
		type: "spline", 
		showInLegend: true,
		yValueFormatString: "Rs##.00",
		lineThickness: 5,
		name: "AS Issued",
		dataPoints: [
			{ label: "Apr-18", y: 0 },
			{ label: "May-18", y: 0 },
			{ label: "Jun-18", y: 0 },
			{ label: "Jul-18", y: 0},
			{ label: "Aug-18", y: 0 },
			{ label: "Sep-18", y: 0 },
			{ label: "Oct-18", y: 1.02 },
			{ label: "Nov-18", y: 1.02 },
			{ label: "Dec-18", y: 1.02 },
			{ label: "Jan-19", y: 3.88 },
			{ label: "Feb-19", y: 46.21 },
			{ label: "Mar-19", y: 67.6 },
			{ label: "Apr-19", y: 67.6 },
			{ label: "May-19", y: 86.1 },
			{ label: "June-19", y: 86.1 },
			{ label: "July-19", y: 557.31 },
			{ label: "Aug-19", y: 559.50 },
			{ label: "Sep-19", y: 559.50},
			{ label: "Oct-19", y: 601.3 },
			{ label: "Nov-19", y: 604.8 },
            { label: "Dec-19", y: 604.8 },            
            { label: "Jan-20", y: 654.8 },              
			{ label: "Feb-20", y: <?php echo $asamt_1920?> }
			
		]
	},
	
	{
		type: "spline", 
		showInLegend: true,
		yValueFormatString: "Rs##.00",
		lineThickness: 5,
		name: "TS Issued",
		dataPoints: [
			{ label: "Apr-18", y: 0 },
			{ label: "May-18", y: 0 },
			{ label: "Jun-18", y: 0 },
			{ label: "Jul-18", y: 0},
			{ label: "Aug-18", y: 0 },
			{ label: "Sep-18", y: 0 },
			{ label: "Oct-18", y: 1.02 },
			{ label: "Nov-18", y: 1.02 },
			{ label: "Dec-18", y: 1.02 },
			{ label: "Jan-19", y: 3.88 },
			{ label: "Feb-19", y: 5.30 },
			{ label: "Mar-19", y: 8.23 },
			{ label: "Apr-19", y: 8.23 },
			{ label: "May-19", y: 13.7 },
			{ label: "June-19", y: 13.7 },
			{ label: "July-19", y: 100.09 },
			{ label: "Aug-19", y: 176.5 },
			{ label: "Sep-19", y: 176.5 },
			{ label: "Oct-19", y: 179.4 },
			{ label: "Nov-19", y: 179.5 },
			{ label: "Dec-19", y: 179.5 },
			{ label: "Jan-20", y: 259.5 },
            { label: "Feb-20", y: <?php echo $tsamt_1920?> }
            
		]
	},
	{
		type: "spline", 
		showInLegend: true,
		yValueFormatString: "Rs##.00",
		lineThickness: 5,
		name: "Tender Issued",
		dataPoints: [
			{ label: "Apr-18", y: 0 },
			{ label: "May-18", y: 0 },
			{ label: "Jun-18", y: 0 },
			{ label: "Jul-18", y: 0},
			{ label: "Aug-18", y: 0 },
			{ label: "Sep-18", y: 0 },
			{ label: "Oct-18", y: 0 },
			{ label: "Nov-18", y: 0.4 },
			{ label: "Dec-18", y: 0.96 },
			{ label: "Jan-19", y: 3.13 },
			{ label: "Feb-19", y: 5.17 },
			{ label: "Mar-19", y: 6.30 },
			{ label: "Apr-19", y: 6.30 },
			{ label: "May-19", y: 6.30 },
			{ label: "June-19", y: 6.30 },
			{ label: "July-19", y: 98.02 },
			{ label: "Aug-19", y: 98.02 },
			{ label: "Sep-19", y: 98.02 },
			{ label: "Oct-19", y: 98.02 },
			{ label: "Nov-19", y: 118.4 },
			{ label: "Dec-19", y: 118.4 },
			{ label: "Jan-20", y: 158.4 },
            { label: "Feb-20", y: <?php echo $tdramt_1920?> }
			
		]
	},
	{
		type: "spline", 
		showInLegend: true,
		yValueFormatString: "Rs##.00",
		lineThickness: 5,
		name: "Work order issued",
		dataPoints: [
			{ label: "Apr-18", y: 0 },
			{ label: "May-18", y: 0 },
			{ label: "Jun-18", y: 0 },
			{ label: "Jul-18", y: 0},
			{ label: "Aug-18", y: 0 },
			{ label: "Sep-18", y: 0 },
			{ label: "Oct-18", y: 0},
			{ label: "Nov-18", y: 0},
			{ label: "Dec-18", y: 0 },
			{ label: "Jan-19", y: 0 },
			{ label: "Feb-19", y: 0.87 },
			{ label: "Mar-19", y: 3.63 },
			{ label: "Apr-19", y: 3.63 },
			{ label: "May-19", y: 4.5},
			{ label: "Jun-19", y: 4.5 },
			{ label: "Jul-19", y: 91.51 },
			{ label: "Aug-19", y: 91.51 },
			{ label: "Sep-19", y: 91.51 },
			{ label: "Oct-19", y: 92.8 },
			{ label: "Nov-19", y: 93 },
			{ label: "Dec-19", y: 93 },
			{ label: "Jan-20", y: 95 },
			{ label: "Feb-20", y: <?php echo $awdamt_1920; ?> }
            
		]
	}]
});
chart1.render();

function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
		e.dataSeries.visible = true;
	} else {
		e.dataSeries.visible = true;
	}
	chart1.render();
}
	
// 14.2.2019 start

	<?php
 
$dataPointsg1 = array(
	array("label"=> "Central Govt Allocatted", "y"=> 500.00),
	array("label"=> "State Govt Allocatted", "y"=> 500.00)
);
$dataPointsg2 = array(
	array("label"=> "Central Govt Allocatted", "y"=> 52.00),
	array("label"=> "State Govt Allocatted", "y"=> 48.00)
);
?>
	
var chart2 = new CanvasJS.Chart("chartContainer2", {
	animationEnabled: true,
	theme: "light2",
	title:{
		text: ""
	},
	legend:{
		cursor: "pointer",
		verticalAlign: "center",
		horizontalAlign: "right",
		itemclick: toggleDataSeries
	},
	data: [{
		type: "column",
		name: "Grant Allocatted",
		indexLabel: "{y}",
        xValueFormatString: "Rs#0.##Cr",
		yValueFormatString: "Rs##Cr",
		showInLegend: true,
		dataPoints: [
			{ label: "500",  y: 500 },
            { label: "500",  y: 500 }
		]
	},{
		type: "column",
		name: "Grant Received",
		indexLabel: "{y}",
        xValueFormatString: "Rs###Cr",
		yValueFormatString: "Rs##Cr",
		showInLegend: true,
		dataPoints: [
			{ label: "52",  y: 52 },
            { label: "48",  y: 48 }
		]
	}]
});
chart2.render();
 
function toggleDataSeries(e2){
	if (typeof(e.dataSeries.visible) === "undefined" || e2.dataSeries.visible) {
		e2.dataSeries.visible = false;
	}
	else{
		e2.dataSeries.visible = true;
	}
	chart2.render();
}
	
//14.2.2019 end 
	
//14.2.2019 part 2 start
var chart3 = new CanvasJS.Chart("chartContainer3", {
	theme:"light2",
	animationEnabled: true,
	title:{
		text: ""
	},
	axisY :{
		includeZero: false,
		title: "Total Expenditure (in INR Cr)",
		prefix: "Rs",
		maximum: 1538,
		interval: 300
	},
	toolTip: {
		shared: "true"
	},
	legend:{
		cursor:"pointer",
		itemclick : toggleDataSeries
	},
	data: [{
		type: "spline",
		visible: true,
		showInLegend: true,
		lineThickness: 15,
		yValueFormatString: "Rs##.00",
		name: "A & OE Utilisation",
		dataPoints: [
			
			{ label: "Apr-18", y: 0.215 },
			{ label: "May-18", y: 0.286 },
			{ label: "Jun-18", y: 0.317 },
			{ label: "Jul-18", y: 0.394 },
			{ label: "Aug-18", y: 0.508 },
			{ label: "Sep-18", y: 0.796 },
			{ label: "Oct-18", y: 1.135 },
			{ label: "Nov-18", y: 1.487 },
			{ label: "Dec-18", y: 1.869 },
			{ label: "Jan-19", y: 2.330 },
			{ label: "Feb-19", y: 2.830 },
			{ label: "Mar-19", y: 3.250 },
			{ label: "Apr-19", y: 4.230 },
			{ label: "May-19", y: 4.520 },
			{ label: "Jun-19", y: 5.50 },
			{ label: "Jul-19", y: 5.50 },
			{ label: "Aug-19", y: 6.22 },
			{ label: "Sep-19", y: 8.08 },
			{ label: "Oct-19", y: 10.34 },            
			{ label: "Nov-19", y: 10.94 },           
			{ label: "Dec-19", y: 11.09 },           
			{ label: "Jan-20", y: 12.46 }
			
			
		]
	},
	{
		type: "spline", 
		showInLegend: true,
		lineThickness: 15,
		yValueFormatString: "Rs##0.000",
		name: "Project Utilisation",
		dataPoints: [
			
			{ label: "Apr-18", y: 0 },
			{ label: "May-18", y: 0 },
			{ label: "Jun-18", y: 0 },
			{ label: "Jul-18", y: 0 },
			{ label: "Aug-18", y: 0 },
			{ label: "Sep-18", y: 0 },
			{ label: "Oct-18", y: 0 },
			{ label: "Nov-18", y: 0 },
			{ label: "Dec-18", y: 0 },
			{ label: "Jan-19", y: 0 },
			{ label: "Feb-19", y: 0 },
			{ label: "Mar-19", y: 0 },
			{ label: "Apr-19", y: 0 },
			{ label: "May-19", y: 0 },
			{ label: "Jun-19", y: 0.0156 },
			{ label: "Jul-19", y: 0.0156 },
			{ label: "Aug-19", y: 0.0195 },
			{ label: "Sep-19", y: 0.0196 },
			{ label: "Oct-19", y: 0.0196 },            
			{ label: "Nov-19", y: 0.0621 },            
			{ label: "Dec-19", y: 0.4430 },            
			{ label: "Jan-20", y: 0.5063 }
			
		]
	}]
});
chart3.render();

function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
		e.dataSeries.visible = false;
	} else {
		e.dataSeries.visible = true;
	}
	chart3.render();
}

	
//	14.2.2019 part 2 end
//	14.2.2019 part 3 start
var chart4 = new CanvasJS.Chart("chartContainer4", {
	theme:"light2",
	animationEnabled: true,
	title:{
		text: ""
	},
	axisY :{
		includeZero: false,
		title: "Total expenditure (in %)",
		preffix: "%",
      minimum: 0,
      indexLabel: "{label} - {y}%",
		maximum: 100,
		interval: 10
	},
	
	toolTip: {
		shared: "true"
	},
	legend:{
		cursor:"pointer",
		itemclick : toggleDataSeries
	},
	data: [{
		type: "spline",
		visible: true,
		lineThickness: 15,
		showInLegend: true,
		
		name: "A & OE Utilisation %",
		dataPoints: [
			
			{ label: "Apr-18", y: 0.014 },
			{ label: "May-18", y: 0.019  },
			{ label: "Jun-18", y: 0.021},
			{ label: "Jul-18", y: 0.026 },
			{ label: "Aug-18", y: 0.0334 },
			{ label: "Sep-18", y: 0.052 },
			{ label: "Oct-18", y: 0.074 },
			{ label: "Nov-18", y: 0.097 },
			{ label: "Dec-18", y: 0.122 },
			{ label: "Jan-19", y: 0.152  },
			{ label: "Feb-19", y:0.184  },
			{ label: "Mar-19", y:0.211  },
			{ label: "Apr-19", y:0.275  },
			{ label: "May-19", y:0.375  },
			{ label: "Jun-19", y:0.368  },
			{ label: "Jul-19", y:0.368  },
			{ label: "Aug-19", y:0.403  },
			{ label: "Sep-19", y:0.527  },
			{ label: "Oct-19", y:0.67  },
            { label: "Nov-19", y:0.71  },
            { label: "Dec-19", y:0.72  },
            { label: "Jan-19", y:0.81  },
            { label: "Feb-20", y:0.81  }
            
			
			
			
		]
	},
	{
		type: "spline", 
		showInLegend: true,
		lineThickness: 15,
		name: "Project Utilisation %",
		dataPoints: [
			
			{ label: "Apr-18", y:0 },
			{ label: "May-18", y:0},
			{ label: "Jun-18", y:0 },
			{ label: "Jul-18", y:0},
			{ label: "Aug-18", y:0 },
			{ label: "Sep-18", y:0 },
			{ label: "Oct-18", y:0 },
			{ label: "Nov-18", y:0 },
			{ label: "Dec-18", y:0 },
			{ label: "Jan-19", y:0 },
			{ label: "Feb-19", y:0 },
			{ label: "Mar-19", y:0 },
			{ label: "Apr-19", y:0 },
			{ label: "May-19", y:0 },
			{ label: "Jun-19", y:0.00104},
			{ label: "Jul-19", y:0.00104},
			{ label: "Aug-19", y:0.0013},
			{ label: "Sep-19", y:0.0013},
			{ label: "Oct-19", y:0.0013},
            { label: "Nov-19", y:0.0043},
            { label: "Dec-19", y:0.029},
            { label: "Jan-20", y:0.033},
            { label: "Feb-20", y:0.033}
			
		]
	}]
});
chart4.render();

function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
		e.dataSeries.visible = false;
	} else {
		e.dataSeries.visible = true;
	}
	chart4.render();
}
	
//	14.2.2019 part 3 end
	}

</script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>

</html>
