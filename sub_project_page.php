<?php
include("dbconfig/dbconfig.php");
$p_id =  $_GET['link'];
$sub_pid = $_GET['link2'];
//echo $p_id;
//echo $sub_pid;

if (!isset($_GET["link2"])) {
    $sub_pid = "NIL";   
}
//echo $sub_pid." ** ".$p_id;

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>SCTL - Project</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.mySlides {display:none;}
</style>
<style>
      .zoomA {
		  z-index: 1;
        width: 200 px;
        height: auto;
        transition-duration: 1s;
        transition-timing-function: ease;
      }
      .zoomA:hover {
        transform: scale(3);
      }
    </style>

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.php" class="logo"><b>SCTL</b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
      
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <?php $userName = $_SESSION["name"]; ?>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="#"><img src="img/sctl_logo.png" class="img-circle" width="80"></a></p>
           <h5 class="centered"><?php echo $_SESSION["name"]; ?></h5>
          <li class="mt">
            <a href="index.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
		  <li class="mt">
            <a href="searchAll.php">
              <i class="fa fa-dashboard"></i>
              <span>All Projects</span>
              </a>
          </li>
		  
		  <li class="mt">
            <a href="searchOngoing.php" >
              <i class="fa fa-dashboard"></i>
              <span>Search Projects</span>
              </a>
          </li>
		  <li class="mt">
            <a href="scp_dpr_completed.php">
              <i class="fa fa-dashboard"></i>
              <span>DPR Completed Projects</span>
              </a>
          </li>
          <li class="mt">
            <a href="imageUpload.php">
              <i class="fa fa-dashboard"></i>
              <span>Image Upload</span>
              </a>
          </li>
          
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3></h3>
        <div class="row mt">
          <div class="col-lg-12">
            
            <?php
			include("search_all_query.php");
					
					
					
					
					$temp = explode(":",$p_id);
					$p_id = $temp[1];
					//echo $temp[0];
					
					
					$sqlProjfund = "SELECT * FROM project_details_with_fund WHERE p_id='".$p_id."'";
					//echo "test".$sqlProjfund;
					$result1 = mysqli_query($conn, $sqlProjfund);
					if(mysqli_num_rows($result1) > 0) {
						$row = mysqli_fetch_assoc($result1);
						$projectName = $row['p_id']."-".$row['p_name'];
						$estCost = round($row['p_estcost'],2);
						$scGrant = round($row['p_grant'],2);
						$cScheme = round($row['p_csp'],2);
						$ppp = round($row['p_ppp'],2);
						$ulbFund = round($row['p_ulb'],2);
						
						}
						
						$sqlas = "SELECT * FROM as_approved WHERE p_id='".$_GET['link']."'";
					//echo $sql;
					$result2 = mysqli_query($conn, $sqlas);
					if(mysqli_num_rows($result2) > 0) {
						$row = mysqli_fetch_assoc($result2);
						$asOrdernum = $row['as_orderNo'];
						$asOrderdate = $row['as_date'];
						$p_type = $row['p_type'];
						$asAmount = $row['as_amt'];
						}
						
						$sqlts = "SELECT * FROM ts_approved WHERE p_id='".$_GET['link']."'";
					//echo $sql;
					$result3 = mysqli_query($conn, $sqlts);
					if(mysqli_num_rows($result3) > 0) {
						$row = mysqli_fetch_assoc($result3);
						$tsOrdernum = $row['ts_ordernum'];
						$tsDate = $row['ts_date'];
						$p_type = $row['p_type'];
						$tsAmount = $row['ts_amt'];
						}
						
						$sqlwa = "SELECT * FROM work_awarded WHERE sub_pid='".$_GET['link']."-1'";
					//echo $sqlwa;
					$result4 = mysqli_query($conn, $sqlwa);
					
					if(mysqli_num_rows($result4) > 0) {
					
						$row = mysqli_fetch_assoc($result4);
						$ctrName = $row['wa_name'];
						$ctrAdd = $row['wa_add'];
						$waAmount = $row['wa_amt'];
						$pStartdate = $row['wa_sdate'];
						$pEnddate = $row['wa_cdate'];
						$wStatus = $row['wa_status'];
						//echo $row['wa_name'];
						}
						
						$sqlallp = "SELECT * FROM allprojects WHERE sub_pid='".$temp[0]."'";
					//echo $sqlallp;
					$resultallp = mysqli_query($conn, $sqlallp);
					
					if(mysqli_num_rows($resultallp) > 0) {
					
						$rowall = mysqli_fetch_assoc($resultallp);
					}
						
			
			?>
			<div class="col-lg-12">
              <div class="col-md-4 col-sm-4 mb" style="width:100%">
                  <div class="green-panel">
                    <div class="green-header">
                      <h3><?php echo $projectName; ?></h3>
                    </div>
					<div class="row black">
                          
						  <div class="col-md-4">
                    <h5><?php echo $wardName; ?></h5>
                    <h5>Estimated Cost(In Rs. Cr) : <?php echo $estCost.""; ?></h5>
					<h5>Smart City Grant(In Rs. Cr) : <?php echo $scGrant; ?></h5>
                    
					</div>
					<div class="col-md-4">
					<h5>PPP(In Rs. Cr) : <?php echo $ppp; ?></h5>
                    <h5>ULB Fund (In Rs. Cr) : <?php echo $ulbFund; ?></h5>
                    
					</div><div class="col-md-4">
                    <h5>Expenditure (In Rs. Cr) : <?php echo $expenditure; ?></h5>
                   <h5>Central Scheme (In Rs. Cr) : <?php echo $cScheme; ?></h5>
					</div>
                    </div>
					
                  </div>
              </div>
            </div>
               
               <div class="col-lg-12">
                   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="custom-box">
                      <!-- WHITE PANEL - TOP USER -->
                      <div class="white-panel">
                        <div class="white-header">
                          <h3>Sub Project Details: </h3>
                          <h3><?php echo $subProjectName ?> </h3>
                        </div>
                        
                        <div class="row black">
                          <div class="col-md-4">
                            <p class="medium mt" style="color:#000">Ward No & Name</p>
                            <p style="font-size:15px color:black"><?php echo $rowall['ward_name']; ?></p>
                          </div>
                          
                         
						  <div class="col-md-3">
                            <p class="medium mt" style="color:#000">FILE NUMBER</p>
                            <p  ><?php echo $fileNum; ?></p>
                          </div>
                          <div class="col-md-3">
                            <p class="medium mt" style="color:#000">Project Status</p>
                            <p  ><?php echo $rowall['p_status']; ?></p>
                          </div>
                         </div>
                          
                         <div class="row black">
						  <div class="col-md-4">
						  	<p class="medium mt" style="color:#000">SCTL In Charge OFFICIAL</p>
                            <p  ><?php echo $sctlOfficial; ?></p>
                          </div>
						  <div class="col-md-4">
						  	<p class="medium mt" style="color:#000">PMC  In Charge OFFICIAL</p>
                            <p  ><?php echo $pmcOfficial; ?></p>
                          </div>
                        </div>
                      </div>
                     </div>
                    
                        <!-- end custombox -->
                        
                      
                  </div>
                  <!-- end col-8 -->
                  
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="custom-box">
					<div class="w3-content w3-section" style="max-width:500px">
					<?php
					$sqlimg = "SELECT * FROM project_images WHERE sub_pid='".$subprojectID."' ";
					//echo $sql;
					$resultimg = mysqli_query($conn, $sqlimg);

					if (mysqli_num_rows($resultimg) > 0) {
						// output data of each row
						while($rowimg = mysqli_fetch_assoc($resultimg)) {
						echo "<img class='mySlides' src='".$rowimg['img_root']."' style='width:100%'>";
						}
						}
					?>
					
				  
				</div>
                     <!-- <img src="img/ui-zac.jpg" height = "204" width ="300"></p>-->
                    </div>
                    <!-- end custombox -->
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="custom-box"> 
                      <!-- WHITE PANEL - TOP USER -->
                      <div class="white-panel">
                        <div class="white-header">
                          <h3>Sanction Details</h3>
                        </div>
                        <div class="row black">
                          
                          <div class="col-md-3">
                            <p class="medium mt" style="color:#000">ADMINISTRATIVE SANCTION ORDER & DATE</p>
                            <p  ><?php echo $rowall['as_orderno']."  ".$rowall['as_date']; ?></p>
                          </div>
						  <div class="col-md-3">
                            <p class="medium mt" style="color:#000" >TECHNICAL SANCTION ORDER NO AND DATE</p>
                            <p  ><?php echo $rowall['ts_orderno']."  ".$rowall['ts_date']; ?></p>
                          </div>
						  <div class="col-md-3">
                            <p class="medium mt"  style="color:#000" >AS AMOUNT (IN Rs.)</p>
                            <p  ><?php echo $rowall['as_amount']." Cr"; ?></p>
                          </div>
						  <div class="col-md-3">
                            <p class="medium mt"  style="color:#000" >TS AMOUNT (IN Rs.)</p>
                            <p  ><?php echo $rowall['ts_amount']." Cr"; ?></p>
                          </div>
						  
                        </div>
                        
                        <div class="row black">
                        	<div class="col-md-3">
                            <p class="medium mt"  style="color:#000" >Tender Order No. & Date</p>
                            <p  ><?php echo $rowall['tender_orderno']; ?></p>
                           </div>
                           <div class="col-md-3">
                            <p class="medium mt"  style="color:#000" >Tender Amount (IN Rs.)</p>
                            <p  ><?php echo $rowall['tender_amount']." Cr"; ?></p>
                           </div>
                           <div class="col-md-3">
                            <p class="medium mt"  style="color:#000" >Tender Start Date</p>
                            <p  ><?php echo $rowall['tender_sdate']; ?></p>
                           </div>
                           <div class="col-md-3">
                            <p class="medium mt"  style="color:#000" >Tender End Date</p>
                            <p  ><?php echo $rowall['tender_cdate']; ?></p>
                           </div>
                        
                        
                        </div>
                        
                        
                        <div class="row black">
						
						   <div class="col-md-3">
                            <p class="medium mt"  style="color:#000">WORK ORDER No & DATE</p>
                            <p  ><?php echo $rowall['wa_orderno']." ".$rowall['wa_date']; ?></p>
                          </div>
						  <div class="col-md-3">
                            <p class="medium mt" style="color:#000" >WORK START DATE</p>
                            <p  ><?php echo $rowall['w_sdate']; ?></p>
                          </div>
						  <div class="col-md-3">
                            <p class="medium mt" style="color:#000" >WORK COMPLETION DATE</p>
                            <p  ><?php echo $rowall['wc_date']; ?></p>
                          </div>
						  <div class="col-md-3">
                            <p class="medium mt" style="color:#000">WORK EXECUTING AGENCY</p>
                            <p ><?php echo $rowall['wa_agency']; ?></p>
                          </div>
						
						</div>
						<div class="row black">
						
						<div class="col-md-3">
                            <p class="medium mt"  style="color:#000">WORK AWARDED AMOUNT (IN Rs.)</p>
                            <p  ><?php echo $rowall['wa_amount']." Cr"; ?></p>
                          </div>
						  
                          
						  						  
						  <div class="col-md-3">
                            <p class="medium mt"  style="color:#000">EXPENDITURE vs AS AMOUNT</p>
                            <p  ><?php echo $rowall['exp_as_cost']; ?></p>
                          </div>
                          <div class="col-md-3">
                            <p class="medium mt"  style="color:#000">EXPENDITURE vs SCP COST</p>
                            <p  ><?php echo $rowall['exp_scp_cost']; ?></p>
                          </div>
						  
                         
						  
                        </div>
                        
						                      
                        <div class="row ">
                          <div class="col-md-12">
                            <p class="medium mt" style="color:#000"  >PROJECT COMPONENTS/DESCRIPTION</p>
                            <p style="text-align:justify" ><?php echo $pDescription; ?></p>
                          </div>
                          
                        </div>
                     </div>
                        
                    </div>
                 </div>
                        <!-- end custombox -->
            </div>
                  <!-- end col-12 -->

                  
                  
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
                        <!-- end custombox -->
          </div>
                  <!-- end col-8 -->
                  
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                     <div class="col-lg-12 mt">
            <div class="row content-panel">
              <div class="panel-heading">
                <ul class="nav nav-tabs nav-justified">
                  <li class="active">
                    <a data-toggle="tab" href="#overview">IMAGES</a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#contact" class="contact-map">DOCUMENTS</a>
                  </li>                 
                </ul>
              </div>
              <!-- /panel-heading -->
              <div class="panel-body">
                <div class="tab-content">
                  <div id="overview" class="tab-pane active">
                  
                  <?php
					$sqlimg = "SELECT * FROM project_images WHERE sub_pid='".$subprojectID."' ";
					//echo $sql;
					$resultimg = mysqli_query($conn, $sqlimg);

					if (mysqli_num_rows($resultimg) > 0) {
						// output data of each row
						while($rowimg = mysqli_fetch_assoc($resultimg)) {
						
						 echo "<div class='col-md-4'><div class='custom-box'><img class='zoomA' src='".$rowimg['img_root']."'style='height:200'' ></p></div></div>";
						}
						}
					?>
                 
                  
                  </div>
                  <!-- /tab-pane -->
                  <div id="contact" class="tab-pane">
                   
                  
                   <?php 
				    
					  $projectImg = "img/ui-zac.jpg";
					  echo "<div class='col-md-3'><a data-toggle='tab' href='#contact'>AS Order</a></div>";
					  echo "<div class='col-md-3'><a data-toggle='tab' href='#contact'>TS Order</a></div>";
				  ?>
                    <!-- end custombox -->
                    
                  </div>
                  <!-- /tab-pane -->
                 
                </div>
                <!-- /tab-content -->
              </div>
              <!-- /panel-body -->
            </div>
            <!-- /col-lg-12 -->
          </div>
                      
                        
                  </div>
                  <!-- end col-12 -->
                  
                  
                  
                  
               </div>
			
			
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <?php include("footer.php");?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="lib/jquery.ui.touch-punch.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->

</body>
<script>
var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myIndex++;
  if (myIndex > x.length) {myIndex = 1}    
  x[myIndex-1].style.display = "block";  
  setTimeout(carousel, 2000); // Change image every 2 seconds
}
</script>

</html>
